//
//  FiltersPSTViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 29/11/23.
//

import UIKit
import SideMenu

class FiltersPSTViewController: UIViewController {
    // MARK: - Properties
    private let navBar = CustomNavBarView(tittle: Constants.locationsPSTText, withBottonLine: 0)
    private lazy var saveButton = UIButton(title: Constants.saveText, titleColor: colorDarkYellow!, font: .montserratBold(ofSize: 16), target: self, action: #selector(savePstsToShow))
    
    private let selectPSTLabel = UILabel(text: Constants.selectPSTText, font: .montserratRegular(ofSize: 14), textColor: colorGrayTextLabel!, numberOfLines: 0)
    
    var pstsArray: [(String, String)] = [("01_agencia_de_viajes","Agencia de viajes"),
                                         ("02_agencia_integradora_Servicios","Agencia integradora servicios"),
                                         ("03_alimentos_bebidas","Alimentos bebidas"),
                                         ("04_arrendadora_autos","Arrendadora autos"),
                                         ("05_balneario_parque_acuatico","Balneario parque acuatico"),
                                         ("06_campo_golf","Campo golf"),
                                         ("07_guardavida","Guardavida"),
                                         ("08_guia_turistas","Guia turistas"),
                                         ("09_hospedaje","Hospedaje"),
                                         ("10_operadora_aventura","Operadora aventura"),
                                         ("11_operadora_buceo","Operadora buceo"),
                                         ("12_operadora_marina","Operadora marina"),
                                         ("13_parque_tematico","Parque tematico"),
                                         ("14_spa","Spa"),
                                         ("15_tiempos_compartidos","Tiempos compartidos"),
                                         ("16_tour_operador","Tour operador"),
                                         ("17_transportadora_turistica","Transportadora turistica"),
                                         ("18_vuelo_en_globo","Vuelo en globo"),
    ]
    
    private let pstscellId = "pstscellId"
    private lazy var pstsAvaliableTableView: UITableView = {
        let tv = UITableView()
        tv.dataSource = self
        tv.delegate = self
        tv.register(MarkersTableViewCell.self, forCellReuseIdentifier: pstscellId)
        tv.separatorInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 10)
        tv.separatorColor = .clear
        tv.backgroundColor = colorBackground
        return tv
    }()
    
    private lazy var pstsAvaliableCollectionView: UICollectionView = {
        let l = UICollectionViewLayout()
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: l)
        
        return cv
    }()
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colorBackground
        setupViewComponents()
    }
    
    // MARK: - Helpers
    private func setupViewComponents() {
        view.addSubview(navBar.withHeight(100))
        navBar.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
        
        saveButton.roundedButton(cornerRadius: 5, color: colorDarkYellow!)
        
        view.addSubview(saveButton.withSize(.init(width: CGFloat( (saveButton.titleLabel?.text!.count)! * 16 ), height: 40)))
        view.addSubview(selectPSTLabel)
        view.addSubview(pstsAvaliableTableView)
        
        saveButton.anchor(top: navBar.bottomAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor, padding: .allSides(16))
        selectPSTLabel.anchor(top: saveButton.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 0, right: 10))
        pstsAvaliableTableView.anchor(top: selectPSTLabel.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 16, left: 0, bottom: 20, right: 0))
        
        navBar.buttonTappedHandler = { [weak self] option in
            if option == "back" {
                self?.navigationController?.popViewController(animated: true)
            }else{
                var menu: SideMenuNavigationController {
                    let vc = MenuViewController()
                    vc.delegate = self
                    let menu = SideMenuNavigationController(rootViewController: vc)
                    menu.settings.menuWidth = (UIScreen.main.bounds.width / 5) * 4
                    menu.presentationStyle = .menuSlideIn
                    menu.statusBarEndAlpha = 0
                    menu.presentationStyle.onTopShadowColor = .lightGray
                    menu.presentationStyle.onTopShadowRadius = 5
                    menu.presentationStyle.onTopShadowOpacity = 0.5
                    menu.presentationStyle.onTopShadowOffset = .zero
                    return menu
                }
                self?.present(menu, animated: true)
            }
        }
    }
    
    
    // MARK: - Selectors
    @objc private func savePstsToShow(){
        
    }
}

//MARK: - UITableViewDataSource
extension FiltersPSTViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pstsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: pstscellId, for: indexPath) as! MarkersTableViewCell
        cell.setupDatCell(tittle: pstsArray[indexPath.row].1, imageName: pstsArray[indexPath.row].0)
        return cell
    }
}
//MARK: - UITableViewDelegate
extension FiltersPSTViewController: UITableViewDelegate {
    
}

//MARK: - MenuViewProtocol
extension FiltersPSTViewController: MenuViewProtocol{
    func menuGoToController(destination: String) {
        GeneralFunctions.shareManager.gotToController(nombreControlador: destination, contexto: self)
    }
}
