//
//  HomeViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 15/11/23.
//

import UIKit
import LBTATools
import MapKit
import GoogleMaps
import CoreLocation
import SideMenu
import MaterialComponents.MaterialTextControls_OutlinedTextFields


class HomeViewController: UIViewController {
    // MARK: - Properties
    private var navBar = CustomNavBarView(tittle: Constants.onboardingTittleText1, withBottonLine: 1)
    
    private var searchTextField = MDCOutlinedTextField()
    private lazy var searchButton: UIButton = {
        let b = UIButton()
        b.setImage(UIImage(systemName: "magnifyingglass")?.withRenderingMode(.alwaysTemplate), for: .normal)
        b.addTarget(self, action: #selector(locationSelector), for: .touchUpInside)
        b.imageView?.contentMode = .scaleAspectFill
        b.tintColor = revertedLabel
        b.backgroundColor = colorView!.withAlphaComponent(0.6)
        b.layer.cornerRadius = 5
        return b
    }()
    private lazy var filtersButton: UIButton = {
        let b = UIButton()
        b.setImage(UIImage(named: "filtersSettings")?.withRenderingMode(.alwaysTemplate), for: .normal)
        b.addTarget(self, action: #selector(filtersSelector), for: .touchUpInside)
        b.imageView?.contentMode = .scaleAspectFill
        b.tintColor = revertedLabel
        b.backgroundColor = colorView!.withAlphaComponent(0.6)
        b.layer.cornerRadius = 5
        return b
    }()
    private lazy var scanButton: UIButton = {
        let b = UIButton()
        b.setImage(UIImage(named: "scanQr")?.withRenderingMode(.alwaysTemplate), for: .normal)
        b.addTarget(self, action: #selector(scanSelector), for: .touchUpInside)
        b.imageView?.contentMode = .scaleAspectFill
        b.tintColor = revertedLabel
        b.backgroundColor = colorView!.withAlphaComponent(0.6)
        b.layer.cornerRadius = 5
        return b
    }()
    private lazy var searchView: UIView = {
        let v = UIView(backgroundColor: .clear)
        searchButton.roundedCorners(cornerRadius: 5, borderWith: 1, corners: [.layerMinXMinYCorner, .layerMinXMaxYCorner])
        filtersButton.roundedCorners(cornerRadius: 5, borderWith: 1, corners: [])
        scanButton.roundedCorners(cornerRadius: 5, borderWith: 1, corners: [.layerMaxXMaxYCorner, .layerMaxXMinYCorner])
        v.hstack(searchTextField,
                 v.hstack(searchButton.withSize(.init(width: 40, height: 40)),
                          filtersButton.withSize(.init(width: 40, height: 40)),
                          scanButton.withSize(.init(width: 40, height: 40))),
                 spacing: 8)
        return v
    }()
    
    private lazy var localizationButton: UIButton = {
        let b = UIButton()
        b.setImage(UIImage(systemName: "location")?.withRenderingMode(.alwaysTemplate), for: .normal)
        b.addTarget(self, action: #selector(locationSelector), for: .touchUpInside)
        b.imageView?.contentMode = .scaleAspectFill
        b.tintColor = .white
        b.backgroundColor = .darkGray.withAlphaComponent(0.5)
        b.layer.cornerRadius = 5
        return b
    }()
    private lazy var sateliteButton: UIButton = {
        let b = UIButton()
        b.setImage(UIImage(systemName: "map")?.withRenderingMode(.alwaysTemplate), for: .normal)
        b.addTarget(self, action: #selector(sateliteSelector), for: .touchUpInside)
        b.imageView?.contentMode = .scaleAspectFill
        b.tintColor = .white
        b.backgroundColor = .darkGray.withAlphaComponent(0.5)
        b.layer.cornerRadius = 5
        return b
    }()
    
    private var locationManager = CLLocationManager()
    private var latitud: Double = 0.0
    private var longitud: Double = 0.0
    
    ///GoogleMaps
    private var mapView: GMSMapView = GMSMapView()
    private var camera: GMSCameraPosition = GMSCameraPosition()
    private var markerLongPress = GMSMarker()
    private var locationsMarkerArray: [GMSMarker] = [] //Comentario: Para las ubicaciones del servicio pendiente.
    
    private var buttonText = UIButton()

    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        updateMapStyle()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocationManager()
        setupViewComponents()
        setupSelectors()
        view.backgroundColor = colorBackground
    }
    deinit{
        NotificationCenter.default.addObserver(self, selector: #selector(updateMapStyle), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Helpers
    private func setupViewComponents(){
        searchTextField.setupMDCOutlinedTextField(placeholder: Constants.startSearchText)
        
        view.addSubview(mapView)
        view.addSubview(searchView)
        view.addSubview(navBar.withHeight(100))
        
        navBar.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
        
        searchView.anchor(top: navBar.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 0, right: 10))
        
        mapView.anchor(top: navBar.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        view.addSubview(sateliteButton.withHeight(40).withWidth(40))
        sateliteButton.anchor(top: searchView.bottomAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 10))
        
        view.addSubview(localizationButton.withHeight(40).withWidth(40))
        localizationButton.anchor(top: sateliteButton.bottomAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 10))
        
        buttonText.setTitle("Demo 0", for: .normal)
        buttonText.backgroundColor = colorRnt
        view.addSubview(buttonText.withSize(.init(width: 100, height: 50)))
        buttonText.anchor(top: nil, leading: nil, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 10))
        buttonText.addTarget(self, action: #selector(demo), for: .touchUpInside)
        
        navBar.buttonTappedHandler = { [weak self] type in
            if type == "back" {
                guard let viewControllers = self?.navigationController?.viewControllers else { return }
                for firstViewController in viewControllers {
                    self?.locationManager.stopUpdatingLocation()
                    if firstViewController is SelectionForLoginViewController {
                        self?.navigationController?.popToViewController(firstViewController, animated: true)
                        break
                    }
                }
            }else{
                var menu: SideMenuNavigationController { 
                    let vc = MenuViewController()
                    vc.delegate = self
                    let menu = SideMenuNavigationController(rootViewController: vc)
                    menu.settings.menuWidth = (UIScreen.main.bounds.width / 5) * 4
                    menu.presentationStyle = .menuSlideIn
                    menu.statusBarEndAlpha = 0
                    menu.presentationStyle.onTopShadowColor = .lightGray
                    menu.presentationStyle.onTopShadowRadius = 5
                    menu.presentationStyle.onTopShadowOpacity = 0.5
                    menu.presentationStyle.onTopShadowOffset = .zero
                    return menu
                }
                self?.present(menu, animated: true)
            }
        }
    }
    
    private func setupLocationManager(){
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        switch locationManager.authorizationStatus {
        case .restricted, .denied:
            let alert = UIAlertController(title: Constants.warningText, message: Constants.openConfigutationsText, preferredStyle: .alert)
            let action = UIAlertAction(title: Constants.aceptText, style: .default,handler: { action in
                GeneralFunctions.shareManager.actionPermisosLocation()
            })
            alert.addAction(action)
            self.present(alert, animated: true)
            break
            
        case .authorizedWhenInUse, .notDetermined:
                locationManager.startUpdatingLocation()
                mapView = GMSMapView.map(withFrame: .zero, camera: camera)
                mapView.isMyLocationEnabled = true
                mapView.delegate = self
            break
        case .authorizedAlways:
            print("No debe entrar aqui")
            break
        default:
            break
        }
        
    }
    
    private func setupSelectors(){
        NotificationCenter.default.addObserver(self, selector: #selector(updateMapStyle), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    // MARK: - Selectors
    @objc private func demo(){
        GeneralFunctions.shareManager.transitionControllerRoot(destination: SelectionForLoginViewController())
    }
    
    @objc private func filtersSelector(_ sender: UIButton){
        navigationController?.pushViewController(FiltersPSTViewController(), animated: true)
    }
    
    @objc private func scanSelector(_ sender: UIButton){
        GeneralFunctions.shareManager.transitionControllerRoot(destination: ScanCertificateViewController())
    }
    
    @objc private func locationSelector(_ sender: UIButton){
        print("Ubicación actual click en botón: \(latitud), \(longitud)")
        camera = GMSCameraPosition.camera(withLatitude: self.latitud, longitude: self.longitud, zoom: 16.0)
        mapView.camera = camera
    }
    
    @objc private func sateliteSelector(_ sender: UIButton){
        if self.mapView.mapType == .normal{
            self.mapView.mapType = .hybrid
            sender.setImage(UIImage(systemName: "map.fill")?.withRenderingMode(.alwaysTemplate), for: .normal)
        }else{
            self.mapView.mapType = .normal
            sender.setImage(UIImage(systemName: "map")?.withRenderingMode(.alwaysTemplate), for: .normal)
        }
    }
    
    @objc private func updateMapStyle(){
        if traitCollection.userInterfaceStyle == .dark {
            mapView.mapStyle = try? GMSMapStyle(jsonString: darkMapStyleJson)
        } else {
            mapView.mapStyle = nil
        }
    }
}


// MARK: - CLLocationManagerDelegate
extension HomeViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        latitud = locValue.latitude
        longitud = locValue.longitude
        print("Ubicación actual: \(latitud), \(longitud)")
        
        // Posicionamos la cámara sobre la ubicación actual
        camera = GMSCameraPosition.camera(withLatitude: latitud, longitude: longitud, zoom: 16.0)
        mapView.camera = camera
        
        manager.stopUpdatingLocation()
        manager.stopMonitoringSignificantLocationChanges()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            print("ERROR: Location updates are not authorized")
            manager.stopMonitoringSignificantLocationChanges()
            return
        }
    }
}

//MARK: - GMSMapViewDelegate
extension HomeViewController: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        markerLongPress.position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        //markerLongPress.title = Constants.selectedLocation
        markerLongPress.map = mapView
        
        camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 16.0)
        mapView.camera = camera
        
        let lat = round(coordinate.latitude * 1_000000) / 1_000000
        let lg = round(coordinate.longitude * 1_000000) / 1_000000
        
        if round(self.latitud * 1_000000) / 1_000000 != lat || round(self.longitud * 1_000000) / 1_000000 != lg {
            localizationButton.setImage(UIImage(systemName: "location")?.withRenderingMode(.alwaysTemplate), for: .normal)
        }else{
            localizationButton.setImage(UIImage(systemName: "location.fill")?.withRenderingMode(.alwaysTemplate), for: .normal)
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("Marker tapped")
        print("Marker tapped informacion: \(marker.userData as Any)")
        let customInfoView = InfoWindowView(data: modelAux.last!)
        customInfoView.frame = CGRect(x: 0, y: 0, width: 200, height: 400)
        customInfoView.center = mapView.projection.point(for: marker.position)
        
        // Asegúrate de ajustar la posición de tu vista para que sea visible
        let yOffset: CGFloat = -250 // Ajusta según sea necesario para que la vista no bloquee completamente el marcador
        //customInfoView.view.center.y += yOffset
        customInfoView.center = view.center
        
        mapView.addSubview(customInfoView)
        //self.present(customInfoView, animated: true)
        
        customInfoView.infoWindowHandler = { option in
            if option == "close"{
                customInfoView.removeFromSuperview()
                //customInfoView.dismiss(animated: true)
            }else if option == "ToDetails"{
                customInfoView.removeFromSuperview()
                let controller = DetailsPSTViewController()
                controller.setupDataInfoWindow(data: modelAux.first!)
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        
        mapView.selectedMarker = marker
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let lat = round(position.target.latitude * 1_000000) / 1_000000
        let lg = round(position.target.longitude * 1_000000) / 1_000000
        
        if round(self.latitud * 1_000000) / 1_000000 != lat || round(self.longitud * 1_000000) / 1_000000 != lg {
            localizationButton.setImage(UIImage(systemName: "location")?.withRenderingMode(.alwaysTemplate), for: .normal)
        }else{
            localizationButton.setImage(UIImage(systemName: "location.fill")?.withRenderingMode(.alwaysTemplate), for: .normal)
        }
    }
    
}

//MARK: - MenuViewProtocol
extension HomeViewController: MenuViewProtocol{
    func menuGoToController(destination: String) {
        locationManager.stopUpdatingLocation()
        GeneralFunctions.shareManager.gotToController(nombreControlador: destination, contexto: self)
    }
}


//MARK: - Maps Helpers
let darkMapStyleJson = """
    [
      {
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#242f3e"
          }
        ]
      },
      {
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#746855"
          }
        ]
      },
      {
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#242f3e"
          }
        ]
      },
      {
        "featureType": "administrative.locality",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#d59563"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#d59563"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#263c3f"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#6b9a76"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#38414e"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#212a37"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9ca5b3"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#746855"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#1f2835"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#f3d19c"
          }
        ]
      },
      {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#2f3948"
          }
        ]
      },
      {
        "featureType": "transit.station",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#d59563"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#17263c"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#515c6d"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#17263c"
          }
        ]
      }
    ]
    """
