//
//  MyProfileViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 04/12/23.
//

import UIKit
import SideMenu
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class MyProfileViewController: UIViewController {
    // MARK: - Prooperties
    let navBar = CustomNavBarView(tittle: Constants.myProfileText, withBottonLine: 1)
    let sscrollView = UIScrollView()
    
    let profileImageView = UIImageView(image: nil)
    lazy var updateProfileImageButton = UIButton(title: Constants.upDateProfileImageText, titleColor: .white, font: .montserratBold(ofSize: 14), backgroundColor: colorRnt!, target: self, action: #selector(updateImageSelector))
    
    let namesLabel = UILabel(text: Constants.namesWithPointsText, font: .montserratBold(ofSize: 14), textColor: .colorGrayTextLabel)
    let namesTextField = MDCOutlinedTextField()
    
    let firstSurnameLabel = UILabel(text: Constants.firstSurnameWithPointsText, font: .montserratBold(ofSize: 14), textColor: .colorGrayTextLabel)
    let firstSurnameTextField = MDCOutlinedTextField()
    
    let secondSurnameLabel = UILabel(text: Constants.secondSurnameWithPointsText, font: .montserratBold(ofSize: 14), textColor: .colorGrayTextLabel)
    let secondSurnameTextField = MDCOutlinedTextField()
    
    let sexLabel = UILabel(text: Constants.sexWithPointsText, font: .montserratBold(ofSize: 14), textColor: .colorGrayTextLabel)
    lazy var sexButton = UIButton(title: "", target: self, action: #selector(sexPicker))
    let sexTextField = MDCOutlinedTextField()
    
    let cellPhoneLabel = UILabel(text: Constants.cellPhoneWithPointsText, font: .montserratBold(ofSize: 14), textColor: .colorGrayTextLabel)
    let cellPhoneTextField = MDCOutlinedTextField()
    
    let emailLabel = UILabel(text: Constants.emailWithPointsText, font: .montserratBold(ofSize: 14), textColor: .colorGrayTextLabel)
    let emailTextField = MDCOutlinedTextField()
    
    let birthdateLabel = UILabel(text: Constants.birthdateWithPointsText, font: .montserratBold(ofSize: 14), textColor: .colorGrayTextLabel)
    lazy var birthdateButton = UIButton(title: "", target: self, action: #selector(birthdatePicker))
    let birthdateTextField = MDCOutlinedTextField()
    
    let civilStatusLabel = UILabel(text: Constants.civilStatusWithPointsText, font: .montserratBold(ofSize: 14), textColor: .colorGrayTextLabel)
    lazy var civilStatusButton = UIButton(title: "", target: self, action: #selector(civilStatusPicker))
    let civilStatusTextField = MDCOutlinedTextField()
    
    let nationalityLabel = UILabel(text: Constants.nationalityWithPointsText, font: .montserratBold(ofSize: 14), textColor: .colorGrayTextLabel)
    let nationalityTextField = MDCOutlinedTextField()
    
    lazy var updateProfileDataButton = UIButton(title: Constants.upDateProfileDataText, titleColor: .white, font: .montserratBold(ofSize: 16), backgroundColor: colorRnt!, target: self, action: #selector(updateDataSelector))

    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colorBackground
        setupViewComponents()
    }
    
    // MARK: - Helpere
    private func setupViewComponents(){
        view.addSubview(navBar.withHeight(100))
        navBar.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
        
        profileImageView.roundedView(cornerRadius: 75, color: .lightGray)
        namesTextField.setupMDCOutlinedTextField(placeholder: Constants.namesText, font: .montserratRegular(ofSize: 15))
        firstSurnameTextField.setupMDCOutlinedTextField(placeholder: Constants.firstSurnameText, font: .montserratRegular(ofSize: 15))
        secondSurnameTextField.setupMDCOutlinedTextField(placeholder: Constants.secondSurnameText, font: .montserratRegular(ofSize: 15))
        sexTextField.setupMDCOutlinedTextField(placeholder: Constants.selectOptionText, font: .montserratRegular(ofSize: 15))
        cellPhoneTextField.setupMDCOutlinedTextField(placeholder: Constants.cellPhoneText, font: .montserratRegular(ofSize: 15))
        emailTextField.setupMDCOutlinedTextField(placeholder: Constants.emailText, font: .montserratRegular(ofSize: 15))
        birthdateTextField.setupMDCOutlinedTextField(placeholder: Constants.selectOptionText, font: .montserratRegular(ofSize: 15))
        civilStatusTextField.setupMDCOutlinedTextField(placeholder: Constants.selectOptionText, font: .montserratRegular(ofSize: 15))
        nationalityTextField.setupMDCOutlinedTextField(placeholder: Constants.nationalityText, font: .montserratRegular(ofSize: 15))
        
        updateProfileImageButton.roundedView(cornerRadius: 5, color: colorRnt!)
        updateProfileDataButton.roundedView(cornerRadius: 5, color: colorRnt!)
        
        let contentView = UIView()
        contentView.stack(contentView.stack(profileImageView.withSize(.init(width: 150, height: 150)),
                                            updateProfileImageButton.withSize(.init(width: 200, height: 40)),
                                            spacing: 8, alignment: .center),
                          UIView().withHeight(20),
                          contentView.stack(namesLabel,
                                            namesTextField.withWidth(Size.width + 32)),
                          contentView.stack(firstSurnameLabel,
                                            firstSurnameTextField.withWidth(Size.width + 32)),
                          contentView.stack(secondSurnameLabel,
                                            secondSurnameTextField.withWidth(Size.width + 32)),
                          contentView.stack(sexLabel,
                                            sexTextField.withWidth(Size.width + 32)),
                          contentView.stack(cellPhoneLabel,
                                            cellPhoneTextField.withWidth(Size.width + 32)),
                          contentView.stack(emailLabel,
                                            emailTextField.withWidth(Size.width + 32)),
                          contentView.stack(birthdateLabel,
                                            birthdateTextField.withWidth(Size.width + 32)),
                          contentView.stack(civilStatusLabel,
                                            civilStatusTextField.withWidth(Size.width + 32)),
                          contentView.stack(nationalityLabel,
                                            nationalityTextField.withWidth(Size.width + 32)),
                          UIView().withHeight(16),
                          updateProfileDataButton.withSize(.init(width: 250, height: 40)),
                          UIView().withHeight(20),
                          spacing: 10,
                          alignment: .center)
        
        contentView.addSubview(sexButton)
        contentView.addSubview(birthdateButton)
        contentView.addSubview(civilStatusButton)
        
        
        sexButton.anchor(top: sexTextField.topAnchor, leading: sexTextField.leadingAnchor, bottom: sexTextField.bottomAnchor, trailing: sexTextField.trailingAnchor)
        sexTextField.rightViewMode = UITextField.ViewMode.always
        let ImageViewDown1 = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        ImageViewDown1.image = UIImage(systemName: "chevron.down")?.withTintColor(.lightGray)
        ImageViewDown1.tintColor = .lightGray
        sexTextField.rightView = ImageViewDown1
        
        birthdateButton.anchor(top: birthdateTextField.topAnchor, leading: birthdateTextField.leadingAnchor, bottom: birthdateTextField.bottomAnchor, trailing: birthdateTextField.trailingAnchor)
        birthdateTextField.rightViewMode = UITextField.ViewMode.always
        let ImageViewDown2 = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        ImageViewDown2.image = UIImage(systemName: "chevron.down")?.withTintColor(.lightGray)
        ImageViewDown2.tintColor = .lightGray
        birthdateTextField.rightView = ImageViewDown2
        
        civilStatusButton.anchor(top: civilStatusTextField.topAnchor, leading: civilStatusTextField.leadingAnchor, bottom: civilStatusTextField.bottomAnchor, trailing: civilStatusTextField.trailingAnchor)
        civilStatusTextField.rightViewMode = UITextField.ViewMode.always
        let ImageViewDown3 = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        ImageViewDown3.image = UIImage(systemName: "chevron.down")?.withTintColor(.lightGray)
        ImageViewDown3.tintColor = .lightGray
        civilStatusTextField.rightView = ImageViewDown3
        
        
        view.addSubview(sscrollView)
        sscrollView.anchor(top: navBar.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 16, left: 0, bottom: 0, right: 0))
        sscrollView.centerXTo(view.centerXAnchor)
        
        sscrollView.addSubview(contentView)
        contentView.anchor(top: sscrollView.topAnchor, leading: sscrollView.leadingAnchor, bottom: sscrollView.bottomAnchor, trailing: sscrollView.trailingAnchor, padding: .init(top: 0, left: 16, bottom: 0, right: 16))
        contentView.centerXTo(sscrollView.centerXAnchor)
        
        navBar.buttonTappedHandler = { [weak self] option in
            if option == "back" {
                guard let viewControllers = self?.navigationController?.viewControllers else { return }
                for firstViewController in viewControllers {
                    if firstViewController is SelectionForLoginViewController {
                        self?.navigationController?.popToViewController(firstViewController, animated: true)
                        break
                    }
                }
            }else{
                var menu: SideMenuNavigationController {
                    let vc = MenuViewController()
                    vc.delegate = self
                    let menu = SideMenuNavigationController(rootViewController: vc)
                    menu.settings.menuWidth = (UIScreen.main.bounds.width / 5) * 4
                    menu.presentationStyle = .menuSlideIn
                    menu.statusBarEndAlpha = 0
                    menu.presentationStyle.onTopShadowColor = .lightGray
                    menu.presentationStyle.onTopShadowRadius = 5
                    menu.presentationStyle.onTopShadowOpacity = 0.5
                    menu.presentationStyle.onTopShadowOffset = .zero
                    return menu
                }
                self?.present(menu, animated: true)
            }
        }
    }
    
    
    // MARK: - Selctors
    @objc private func updateImageSelector(){
        print("updateImageSelector")
    }
    @objc private func updateDataSelector(){
        print("updateDataSelector ---")
    }
    
    @objc private func sexPicker(_ sender: UIButton){
        sender.setTitle(Constants.selectOptionText, for: .normal)
        sender.titleLabel?.font = .montserratRegular(ofSize: 15)
        GeneralFunctions.shareManager.pickerDataSelection(tittle: Constants.sexText, rows: ["Maculino", "Femenino"], sender: sender)
        sexTextField.placeholder = ""
        
        sender.contentHorizontalAlignment = .leading
        sender.setTitleColor(colorGrayTextPlaceholder!, for: .normal)
    }
    @objc private func birthdatePicker(_ sender: UIButton){
        sender.setTitle(Constants.selectOptionText, for: .normal)
        sender.titleLabel?.font = .montserratRegular(ofSize: 15)
        GeneralFunctions.shareManager.pickerDatesSelection(tittle: Constants.birthdateText, sender: sender)
        birthdateTextField.placeholder = ""
        
        sender.contentHorizontalAlignment = .leading
        sender.setTitleColor(colorGrayTextPlaceholder!, for: .normal)
    }
    @objc private func civilStatusPicker(_ sender: UIButton){
        sender.setTitle(Constants.selectOptionText, for: .normal)
        sender.titleLabel?.font = .montserratRegular(ofSize: 15)
        GeneralFunctions.shareManager.pickerDataSelection(tittle: Constants.civilStatusText, rows: ["Soltero", "Casado"], sender: sender)
        civilStatusTextField.placeholder = ""
        
        sender.contentHorizontalAlignment = .leading
        sender.setTitleColor(colorGrayTextPlaceholder!, for: .normal)
    }

}

//MARK: - MenuViewProtocol
extension MyProfileViewController: MenuViewProtocol{
    func menuGoToController(destination: String) {
        GeneralFunctions.shareManager.gotToController(nombreControlador: destination, contexto: self)
    }
}
