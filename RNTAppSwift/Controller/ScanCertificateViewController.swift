//
//  ScanCertificateViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 24/11/23.
//

import UIKit
import Photos
import SideMenu
import AVFoundation

class ScanCertificateViewController: UIViewController {
    // MARK: - Properties
    let navBar = CustomNavBarView(tittle: Constants.scanCodeQrText, withBottonLine: 1)
    let barImageView = UIImageView(image: UIImage(named: "barImage"), contentMode: .scaleAspectFill )
    let scanCodeQrLabel = UILabel(text: Constants.scanCodeQrText, font: .NunitoSansBlack(ofSize: 30), textColor: .link, textAlignment: .center)
    
    let scanCodeQrImageView = UIImageView(image: UIImage(named: "scanCodeQRPhone")?.withTintColor(UIColor.revertedLabel), contentMode: .scaleAspectFit)
    lazy var scanCodeQrButton = UIButton(title: Constants.startScanerText, titleColor: .white, font: .montserratRegular(ofSize: 16), backgroundColor: colorRnt!, target: self, action: #selector(openCameraSelector))
    lazy var scanCodeQrFormImageButton = UIButton(title: Constants.scanerFromImageText, titleColor: .link, font: .montserratRegular(ofSize: 16), target: self, action: #selector(openGalerySelector))
    let codeScanedLabel = UILabel(text: "Resultado de escanear: ", font: .montserratRegular(ofSize: 12), textColor: UIColor.revertedLabel, numberOfLines: 0)
    lazy var contentView: UIView = {
        let v = UIView(backgroundColor: colorBackground!)
        scanCodeQrImageView.tintColor = UIColor.revertedLabel
        v.addSubview(scanCodeQrImageView.withSize(.init(width: Size.width, height: Size.width)))
        scanCodeQrImageView.anchor(top: v.topAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 50, left: 0, bottom: 0, right: 0))
        scanCodeQrImageView.centerXTo(v.centerXAnchor)
        
        v.addSubview(scanCodeQrButton.withWidth( CGFloat((scanCodeQrButton.titleLabel?.text!.count)! * 16) ))
        scanCodeQrButton.anchor(top: scanCodeQrImageView.bottomAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 20, left: 10, bottom: 0, right: 10))
        scanCodeQrButton.centerXTo(v.centerXAnchor)
        scanCodeQrButton.roundedButton(cornerRadius: 5, color: colorRnt!)
        
        v.addSubview(scanCodeQrFormImageButton.withWidth(Size.width))
        scanCodeQrFormImageButton.anchor(top: scanCodeQrButton.bottomAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 20, left: 10, bottom: 0, right: 10))
        scanCodeQrFormImageButton.centerXTo(v.centerXAnchor)
        scanCodeQrFormImageButton.textUnderLined()
        
        v.addSubview(codeScanedLabel)
        codeScanedLabel.anchor(top: scanCodeQrFormImageButton.bottomAnchor, leading: v.leadingAnchor, bottom: nil, trailing: v.trailingAnchor, padding: .allSides(16))
        
        return v
    }()

    var scannerViewController: ScannerViewController!
    var imagePickerController: UIImagePickerController!
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colorBackground
        setupViewComponents()
        scannerViewController = ScannerViewController()
        scannerViewController.delegate = self
        scannerViewController.setupCamera()
        
        setupImagePicker()
        imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
    }
    
    // MARK: - Helpers
    private func setupViewComponents(){
        let styledString = NSMutableAttributedString(string: Constants.scanCodeQrText)
        styledString.addAttributes([.strokeColor: UIColor.white, .foregroundColor: UIColor.colorRnt, .strokeWidth: -5.0], range: NSMakeRange(0, styledString.length))
        scanCodeQrLabel.attributedText = styledString
        scanCodeQrLabel.adjustsFontSizeToFitWidth = true
        scanCodeQrLabel.isHidden = true
        
        barImageView.addSubview(scanCodeQrLabel)
        scanCodeQrLabel.anchor(top: barImageView.topAnchor, leading: barImageView.leadingAnchor, bottom: barImageView.bottomAnchor, trailing: barImageView.trailingAnchor)
        barImageView.roundedCorners(cornerRadius: 10, corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner])
        barImageView.isHidden = true
        
        view.addSubview(navBar.withHeight(100))
        navBar.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
        
        view.addSubview(barImageView.withHeight(50))
        barImageView.anchor(top: navBar.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 10, left: 8, bottom: 0, right: 8))
        
        view.addSubview(contentView)
        contentView.anchor(top: barImageView.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 8, bottom: 10, right: 8))
        
        navBar.buttonTappedHandler = { [weak self] option in
            if option == "back" {
                guard let viewControllers = self?.navigationController?.viewControllers else { return }
                for firstViewController in viewControllers {
                    if firstViewController is SelectionForLoginViewController {
                        self?.navigationController?.popToViewController(firstViewController, animated: true)
                        break
                    }
                }
            }else{
                var menu: SideMenuNavigationController {
                    let vc = MenuViewController()
                    vc.delegate = self
                    let menu = SideMenuNavigationController(rootViewController: vc)
                    menu.settings.menuWidth = (UIScreen.main.bounds.width / 5) * 4
                    menu.presentationStyle = .menuSlideIn
                    menu.statusBarEndAlpha = 0
                    menu.presentationStyle.onTopShadowColor = .lightGray
                    menu.presentationStyle.onTopShadowRadius = 5
                    menu.presentationStyle.onTopShadowOpacity = 0.5
                    menu.presentationStyle.onTopShadowOffset = .zero
                    return menu
                }
                self?.present(menu, animated: true)
            }
        }
    }
    
    private func setupImagePicker(){
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        
        switch photoAuthorizationStatus {
        case .authorized:
            print("Autorizado")
        case .notDetermined, .denied, .restricted:

            PHPhotoLibrary.requestAuthorization { (status) in
                if status == .authorized {
                    print("Autorizado")
                } else {
                    print("photoAuthorizationStatus: ", photoAuthorizationStatus)
                }
            }
        case .limited:
            print("photoAuthorization_Status: ", photoAuthorizationStatus)
        @unknown default:
            break
        }
    }
    
    //MARK: - Selectors
    @objc private func openCameraSelector(){
        scannerViewController.startScanning()
        present(scannerViewController, animated: true, completion: nil)
    }
    @objc private func openGalerySelector(){
        present(imagePickerController, animated: true, completion: nil)
    }
}
//MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate
extension ScanCertificateViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        
        if let pickedImage = info[.originalImage] as? UIImage {
            // Here you can use the pickedImage for further processing
            // For example, you can pass it to your scannerViewController for handling
            scannerViewController.processImage(pickedImage)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension ScanCertificateViewController: ScannerViewControllerProtocol {
    func infoRecoveredFromScanner(info: String, camera: Bool) {
        if camera{
            codeScanedLabel.text = "Resultado de escanear: " + info
        }else{
            codeScanedLabel.text = "Resultado de escanear imagen: " + info
        }
        
    }
}

//MARK: - MenuViewProtocol
extension ScanCertificateViewController: MenuViewProtocol{
    func menuGoToController(destination: String) {
        GeneralFunctions.shareManager.gotToController(nombreControlador: destination, contexto: self)
    }
}
