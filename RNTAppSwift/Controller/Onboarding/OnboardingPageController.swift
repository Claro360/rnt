//
//  OnboardingPageController.swift
//  Claro360-Swift
//
//  Created by Alberto Garcia on 22/05/20.
//  Copyright © 2020 GlobalCorporation. All rights reserved.
//

import UIKit
import LBTATools

class OnboardingPageController: UIViewController {
    // MARK: - Properties
    private let imageView = UIImageView(image: nil, contentMode: .scaleToFill)
    private let titleLabel = UILabel(font: .montserratBold(ofSize: 25), textColor: colorRnt!, textAlignment: .center, numberOfLines: 0)
    
    private let textView = UITextView(text: Constants.dumieLongText, font: .montserratBold(ofSize: 14), textColor: .label, textAlignment: .justified)
    
    private lazy var aceptarButton = UIButton(title: Constants.startText, titleColor: colorRnt!, font: .montserratBold(ofSize: 14), target: self, action: #selector(aceptarTerminos))
    
    var aceptar: (() -> Void)?
    
    // MARK: - Init
    init(title: String, description: String, image: UIImage, visibleButton: Bool) {
        super.init(nibName: nil, bundle: nil)
        self.imageView.image = image
        if visibleButton {
            aceptarButton.isHidden = false
        } else {
            aceptarButton.isHidden = true
        }
        self.titleLabel.text = title
        self.textView.text = description
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewComponents()
    }
    
    // MARK: - Helpers
    fileprivate func setupViewComponents() {
        view.backgroundColor = colorBackground
        
        aceptarButton.roundedButton(cornerRadius: 5, color: colorRnt!)
        textView.allowsEditingTextAttributes = false
        textView.isSelectable = false
        textView.backgroundColor = colorBackground
        
        let containerView = UIView()
        
        containerView.stack(UIView().withHeight(20),
                            titleLabel.withWidth(view.frame.width - 64),
                            imageView.withSize(.init(width: Size.width, height: Size.width * 0.7)),
                            textView.withWidth(view.frame.width - 32),
                            aceptarButton.withSize(.init(width: 150, height: 40)),
                            spacing: 26,
                            alignment: .center)
        
        view.addSubview(containerView)
        containerView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor)
    }
    
    // MARK: - Selectors
    @objc fileprivate func aceptarTerminos() {
        aceptar?()
    }
}
