//
//  OnboardingPageViewController.swift
//  Claro360-Swift
//
//  Created by Alberto Garcia on 22/05/20.
//  Copyright © 2020 GlobalCorporation. All rights reserved.
//

import UIKit

class OnboardingPageViewController: UIViewController {
    //MARK: - Properties
    private var pageViewController: UIPageViewController!
    var controllers = [UIViewController]()
    var lastController = OnboardingPageController(title: " ", description: "", image: UIImage(named: "Onboarding_gt_2")!, visibleButton: true)
    var destinationController = ""//UIViewController()
    
    private lazy var cancelButton = UIButton(image: UIImage(systemName: "xmark")!, target: self, action: #selector(closeSelector))
    
    //MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colorBackground
        setupViewComponents()
    }
    
    //MARK: - Helpers
    private func setupViewComponents() {
        controllers.append(lastController)
        
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.dataSource = self
        pageViewController.view.backgroundColor = colorBackground
        
        addChild(pageViewController)
        view.addSubview(pageViewController.view)
        pageViewController.view.fillSuperview()
        
        view.addSubview(cancelButton.withSize(.init(width: 50, height: 50)))
        cancelButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor)
        
        lastController.aceptar = { [weak self] in
            if (UserDefaults.standard.bool(forKey: "firstTime")){
                self?.dismiss(animated: true, completion: nil)
                UserDefaults.standard.set(false, forKey: "firstTime")
            }else{
                if self!.destinationController == "HomeViewController" {
                    self?.navigationController?.pushViewController(HomeViewController(), animated: true)
                }else{
                    self?.navigationController?.pushViewController(TravelServicesSignInViewController(), animated: true)
                }
                
            }
        }
        pageViewController.setViewControllers([controllers[0]], direction: .forward, animated: true, completion: nil)
    }
    
    //MARK: - Selectors
    @objc fileprivate func closeSelector() {
        /*if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
           let window = windowScene.windows.filter({ $0.isKeyWindow }).first {
            window.rootViewController = UINavigationController(rootViewController: destinationController)
        }*/
        if self.destinationController == "HomeViewController" {
            self.navigationController?.pushViewController(HomeViewController(), animated: true)
        }else{
            self.navigationController?.pushViewController(TravelServicesSignInViewController(), animated: true)
        }
    }
}

//MARK: - UIPageViewControllerDataSource
extension OnboardingPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let index = controllers.firstIndex(of: viewController) {
            if index > 0 {
                return controllers[index - 1]
            } else {
                return nil
            }
        }

        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let index = controllers.firstIndex(of: viewController) {
            if index < controllers.count - 1 {
                return controllers[index + 1]
            } else {
                return nil
            }
        }

        return nil
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return controllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}
