//
//  LogIntoViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 15/11/23.
//

import UIKit
import LBTATools
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class SignInViewController: LBTAFormController {
    // MARK: - Properties
    private var loginImageView = UIImageView(image: UIImage(named: "LaunchS"), contentMode: .scaleAspectFit)
    private var welcomeLabel = UILabel(text: Constants.welcomeToSECTURText, font: .montserratBold(ofSize: 25), textColor: colorRnt!, textAlignment: .center)
    private var loginLabel = UILabel(text: Constants.signIn_LabelWithPointsText, font: .montserratBold(ofSize: 15), textColor: colorRnt!, textAlignment: .center)
    
    private var userTextField = MDCOutlinedTextField()
    private var passTextField = MDCOutlinedTextField()
    private var confirmPassTextField = MDCOutlinedTextField()
    
    private var termsLabel = UILabel(text: Constants.termsText, textColor: .label, textAlignment: .center, numberOfLines: 0)
    private lazy var signInButton = UIButton(title: Constants.signInText, titleColor: colorRnt!, font: .montserratBold(ofSize: 15), target: self, action: #selector(logInTo))
    
    private var accountLabel = UILabel(text: Constants.accountText, textColor: .label, textAlignment: .center)
    private lazy var toLoginButton = UIButton(title: Constants.loginText, titleColor: .link, font: .montserratBold(ofSize: 13), target: self, action: #selector(toLogIn))
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colorBackground
        //view.addGradientColor(colors: [colorBackground!, colorBrownRnt!])
        setupViewComponents()
    }
    
    // MARK: - Helpers
    private func setupViewComponents(){
        userTextField.setupMDCOutlinedTextField(placeholder: Constants.userText, labelTitle: Constants.emailWithPointsText)
        passTextField.setupMDCOutlinedTextField(placeholder: Constants.passwordText, labelTitle: Constants.passwordWithPointsText)
        confirmPassTextField.setupMDCOutlinedTextField(placeholder: Constants.confirmPasswordText, labelTitle: Constants.confirmPasswordWithPointsText)
        
        userTextField.delegate = self
        passTextField.delegate = self
        confirmPassTextField.delegate = self
        
        passTextField.trailingView = passTextField.addButtonHidenPass()
        confirmPassTextField.trailingView = confirmPassTextField.addButtonHidenPass()
        
        signInButton.roundedButton(cornerRadius: 5, color: colorRnt!, borderWidth: 2)
        toLoginButton.textUnderLined()
        
        let containerView = UIView()
        containerView.stack(UIView(backgroundColor: colorBackground!).withHeight(10),
                            loginImageView.withSize(.init(width: Size.width, height: Size.width / 2)),
                            UIView(backgroundColor: colorBackground!).withHeight(10),
                            welcomeLabel,
                            UIView(backgroundColor: colorBackground!).withHeight(10),
                            loginLabel,
                            containerView.stack(userTextField.withWidth(Size.width),
                                                passTextField.withWidth(Size.width),
                                                confirmPassTextField.withWidth(Size.width), spacing: 10),
                            containerView.stack(termsLabel).withMargins(.init(top: 0, left: 32, bottom: 0, right: 32)),
                            signInButton.withSize(.init(width: (Size.width + 64) / 2 , height: 40)),
                            containerView.hstack(accountLabel, toLoginButton, spacing: 8),
                            UIView(backgroundColor: colorBackground!).withHeight(20),
                            spacing: 20,
                            alignment: .center)
        
        formContainerStackView.addArrangedSubview(containerView)
    }
    
    // MARK: - Selectors
    @objc private func toLogIn(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func logInTo(){
        let alert = UIAlertController(title: Constants.recordText, message: Constants.onProgresText, preferredStyle: .alert)
        alert.addAction(.init(title: Constants.aceptText, style: .default))
        self.present(alert, animated: true)
    }
}

extension SignInViewController: UITextFieldDelegate {
    
}
