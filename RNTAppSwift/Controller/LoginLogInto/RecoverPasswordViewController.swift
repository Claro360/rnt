//
//  RecoverPasswordViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 16/11/23.
//

import UIKit
import LBTATools
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class RecoverPasswordViewController: LBTAFormController {
    //MARK: - Properties
    private lazy var return_Button: UIButton = {
        let button = UIButton(type: .system)
        let arrowImage = UIImage(systemName: "chevron.left")?.withRenderingMode(.alwaysTemplate)
        
        let resizedImage = arrowImage?.resizableImage(withCapInsets: .zero, resizingMode: .stretch)
        button.setImage(resizedImage, for: .normal)
        button.addTarget(self, action: #selector(toLogin), for: .touchUpInside)
        return button
    }()
    
    
    private var loginImageView = UIImageView(image: UIImage(named: "LaunchS"), contentMode: .scaleAspectFit)
    private var recoverPasswordLabel = UILabel(text: Constants.recoverPasswordText, font: .montserratBold(ofSize: 25), textColor: colorRnt!, textAlignment: .center)
    
    private var userTextField = MDCOutlinedTextField()
    
    private lazy var recoverButton = UIButton(title: Constants.recoverText, titleColor: colorRnt!, font: .montserratBold(ofSize: 15), target: self, action: #selector(recoverPasswordSelector))
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colorBackground
        //view.addGradientColor(colors: [colorBackground!, colorBrownRnt!])
        setupViewComponents()
    }
    
    private func setupViewComponents(){
        return_Button.tintColor = UIColor.colorGrayTextButton
        return_Button.imageView?.contentMode = .scaleToFill
        return_Button.backgroundColor = UIColor.clear
        
        userTextField.setupMDCOutlinedTextField(placeholder: Constants.userText, labelTitle: Constants.emailWithPointsText)
        userTextField.delegate = self
        
        recoverButton.roundedButton(cornerRadius: 5, color: colorRnt!, borderWidth: 2)
        
        let containerView = UIView()
        containerView.stack(UIView(backgroundColor: colorBackground!).withHeight(10),
                            loginImageView.withSize(.init(width: Size.width, height: Size.width / 2)),
                            UIView(backgroundColor: colorBackground!).withHeight(10),
                            recoverPasswordLabel,
                            UIView(backgroundColor: colorBackground!).withHeight(10),
                            userTextField.withWidth(Size.width),
                            recoverButton.withSize(.init(width: (Size.width + 64) / 2 , height: 40)),
                            UIView(backgroundColor: colorBackground!).withHeight(20),
                            spacing: 20,
                            alignment: .center)
        
        formContainerStackView.addArrangedSubview(containerView)
        
        view.addSubview(return_Button.withSize(.init(width: 50, height: 50)))
        return_Button.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 20, left: 10, bottom: 0, right: 0))
    }

    // MARK: - Selectors
    @objc private func recoverPasswordSelector(){
        let alert = UIAlertController(title: Constants.recoverText, message: Constants.onProgresText, preferredStyle: .alert)
        alert.addAction(.init(title: Constants.aceptText, style: .default))
        self.present(alert, animated: true)
    }
    
    @objc private func toLogin(){
        navigationController?.popViewController(animated: true)
    }
}

extension RecoverPasswordViewController: UITextFieldDelegate {
    
}
