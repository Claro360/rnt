//
//  LoginViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 15/11/23.
//

import UIKit
import LBTATools
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class LoginViewController: LBTAFormController {
    //MARK: - Properties
    private var loginImageView = UIImageView(image: UIImage(named: "LaunchS"), contentMode: .scaleAspectFit)
    private var welcomeLabel = UILabel(text: Constants.welcomeToSECTURText, font: .montserratBold(ofSize: 25), textColor: colorRnt!, textAlignment: .center)
    private var getIntoLabel = UILabel(text: Constants.getIntoWithPointsText, font: .montserratBold(ofSize: 15), textColor: colorRnt!, textAlignment: .center)
    
    private var userTextField = MDCOutlinedTextField()
    private var passTextField = MDCOutlinedTextField()
    
    private lazy var loginButton = UIButton(title: Constants.loginText, titleColor: colorRnt!, font: .montserratBold(ofSize: 15), target: self, action: #selector(toSelectionForLoginViewController))
    private lazy var logInToButton = UIButton(title: Constants.signInText, titleColor: colorGrayTextButton!, font: .montserratBold(ofSize: 15), target: self, action: #selector(toLogInTo))
    private lazy var recoverPasswordButton = UIButton(title: Constants.forgotPassword, titleColor: .link, font: .montserratBold(ofSize: 13), target: self, action: #selector(toRecoverPassword))
    
    var toController: String = "LoginViewController"

    //MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colorBackground
        //view.addGradientColor(colors: [colorBackground!, colorBrownRnt!])
        goToController()
        setupViewComponents()
    }
    
    init(toController: String = "LoginViewController"){
        self.toController = toController
        super.init()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helpers
    private func setupViewComponents(){
        userTextField.setupMDCOutlinedTextField(placeholder: Constants.userText, labelTitle: Constants.emailWithPointsText)
        passTextField.setupMDCOutlinedTextField(placeholder: Constants.passwordText, labelTitle: Constants.passwordWithPointsText)
        
        userTextField.delegate = self
        passTextField.delegate = self
        
        passTextField.trailingView = passTextField.addButtonHidenPass()
        
        loginButton.roundedButton(cornerRadius: 5, color: colorRnt!, borderWidth: 2)
        logInToButton.roundedButton(cornerRadius: 5, color: colorGrayTextButton!, borderWidth: 2)
        
        recoverPasswordButton.textUnderLined()
        
        let containerView = UIView()
        containerView.stack(UIView().withHeight(10),
                            loginImageView.withSize(.init(width: Size.width, height: Size.width / 2)),
                            UIView().withHeight(10),
                            welcomeLabel,
                            UIView().withHeight(10),
                            getIntoLabel,
                            containerView.stack(userTextField.withWidth(Size.width),
                                                passTextField.withWidth(Size.width), spacing: 10),
                            loginButton.withSize(.init(width: (Size.width + 64) / 2 , height: 40)),
                            logInToButton.withSize(.init(width: (Size.width + 64) / 2 , height: 40)),
                            recoverPasswordButton,
                            UIView().withHeight(20),
                            spacing: 20,
                            alignment: .center)
        
        formContainerStackView.addArrangedSubview(containerView)
        
        loginButton.backgroundColor = .clear
        loginButton.layer.sublayers?.first?.frame = loginButton.bounds
        loginButton.addGradientColor(colors: [.systemRed, .systemOrange], vertial: false)
        
    }
    
    private func goToController(){
        if toController == "LoginViewController" {
            print("LoginViewController")
        }else if toController == "SignInViewController" {
            navigationController?.pushViewController(SignInViewController(), animated: true)
        }
    }

    // MARK: - Selectors
    @objc private func toLogInTo(){
        navigationController?.pushViewController(SignInViewController(), animated: true)
    }

    @objc private func toRecoverPassword(){
        navigationController?.pushViewController(RecoverPasswordViewController(), animated: true)
    }

    @objc private func toSelectionForLoginViewController() {
        navigationController?.pushViewController(HomeViewController(), animated: true)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        /*if textField.text?.count ?? 0 >= 1 {
            if textField == passTextField {
                passTextField.setTextColor(.label, for: .normal)
            }else{
                userTextField.setTextColor(.label, for: .normal)
            }
        }*/
    }
}
