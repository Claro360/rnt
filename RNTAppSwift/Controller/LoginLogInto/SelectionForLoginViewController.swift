//
//  SelectionForLoginViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 16/11/23.
//

import UIKit

class SelectionForLoginViewController: UIViewController {
    //MARK: - Properties
    private var loginImageView = UIImageView(image: UIImage(named: "LaunchS"), contentMode: .scaleAspectFit)
    private var welcomeLabel = UILabel(text: Constants.welcomeToSECTURText, font: .montserratBold(ofSize: 25), textColor: colorRnt!, textAlignment: .center)
    private var loginAsLabel = UILabel(text: Constants.loginAsWithPointsText, font: .montserratBold(ofSize: 15), textColor: colorRnt!, textAlignment: .center)
    
    private lazy var touristButton = UIButton(title: Constants.tourisText, titleColor: .white, font: .montserratBold(ofSize: 15), backgroundColor: colorRnt!, target: self, action: #selector(toTourisViewController))
    private lazy var guestButton = UIButton(title: Constants.guestText, titleColor: colorRnt!, font: .montserratBold(ofSize: 15), target: self, action: #selector(toGuestViewController))
    private lazy var travelServicesButton = UIButton(title: Constants.travelServicesText, titleColor: colorRnt!, font: .montserratBold(ofSize: 15), target: self, action: #selector(toTravelServicesViewController))

    //MARK: - Properties
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //let CLLocationManager().locationManager.stopUpdatingLocation()
        setupViewComponents()
    }

    // MARK: - Helpers
    private func setupViewComponents(){
        view.backgroundColor = colorBackground
        
        touristButton.roundedButton(cornerRadius: 5, color: colorRnt!, borderWidth: 2)
        guestButton.roundedButton(cornerRadius: 5, color: colorRnt!, borderWidth: 2)
        travelServicesButton.roundedButton(cornerRadius: 5, color: colorRnt!, borderWidth: 2)
        
        let containerView = UIView()
        containerView.stack(loginImageView.withSize(.init(width: Size.width + 32, height: Size.width / 2)),
                            UIView(backgroundColor: colorBackground!).withHeight(10),
                            welcomeLabel,
                            UIView(backgroundColor: colorBackground!).withHeight(10),
                            loginAsLabel,
                            UIView(backgroundColor: colorBackground!).withHeight(1),
                            containerView.stack(touristButton.withHeight(40),
                                                guestButton.withHeight(40),
                                                travelServicesButton.withHeight(40), spacing: 20),
                            spacing: 10)
        view.addSubview(containerView)
        containerView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 20, left: 16, bottom: 0, right: 16))
        
    }
    
    // MARK: - Selectors
    @objc private func toTourisViewController(){
        let controller = OnboardingPageViewController()
        controller.controllers = [OnboardingPageController(title: Constants.onboardingTittleText1, description: Constants.onboardingDescriptionText1, image: UIImage(named: "Onboarding_gt_1")!, visibleButton: false),
                                  OnboardingPageController(title: Constants.onboardingTittleText2, description: Constants.onboardingDescriptionText2, image: UIImage(named: "Onboarding_gt_2")!, visibleButton: false),
                                  OnboardingPageController(title: Constants.onboardingTittleText3, description: Constants.onboardingDescriptionText3, image: UIImage(named: "Onboarding_gt_3")!, visibleButton: false)]
        controller.lastController = OnboardingPageController(title: Constants.onboardingTittleText4, description: Constants.onboardingDescriptionText4, image: UIImage(named: "Onboarding_gt_4")!, visibleButton: true)
        controller.destinationController = "HomeViewController"
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc private func toGuestViewController(){
        let controller = OnboardingPageViewController()
        controller.controllers = [OnboardingPageController(title: Constants.onboardingTittleText1, description: Constants.onboardingDescriptionText1, image: UIImage(named: "Onboarding_gt_1")!, visibleButton: false),
                                  OnboardingPageController(title: Constants.onboardingTittleText2, description: Constants.onboardingDescriptionText2, image: UIImage(named: "Onboarding_gt_2")!, visibleButton: false),
                                  OnboardingPageController(title: Constants.onboardingTittleText3, description: Constants.onboardingDescriptionText3, image: UIImage(named: "Onboarding_gt_3")!, visibleButton: false)]
        controller.lastController = OnboardingPageController(title: Constants.onboardingTittleText4, description: Constants.onboardingDescriptionText4, image: UIImage(named: "Onboarding_gt_4")!, visibleButton: true)
        controller.destinationController = "HomeViewController"
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc private func toTravelServicesViewController(){
        let controller = OnboardingPageViewController()
        controller.controllers = [OnboardingPageController(title: Constants.onboardingTittleText5, description: Constants.onboardingDescriptionText5, image: UIImage(named: "Onboarding_ts_1")!, visibleButton: false),
                                  OnboardingPageController(title: Constants.onboardingTittleText6, description: Constants.onboardingDescriptionText6, image: UIImage(named: "Onboarding_ts_2")!, visibleButton: false),
                                  OnboardingPageController(title: Constants.onboardingTittleText7, description: Constants.onboardingDescriptionText7, image: UIImage(named: "Onboarding_ts_3")!, visibleButton: false)]
        controller.lastController = OnboardingPageController(title: Constants.onboardingTittleText8, description: Constants.onboardingDescriptionText8, image: UIImage(named: "Onboarding_ts_4")!, visibleButton: true)
        controller.destinationController = "TravelServicesSignInViewController"
        navigationController?.pushViewController(controller, animated: true)
    }
    
}
