//
//  ScannerViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 28/11/23.
//

import UIKit
import AVFoundation

protocol ScannerViewControllerProtocol {
    func infoRecoveredFromScanner(info: String, camera: Bool)
}
 
class ScannerViewController: UIViewController {
    // MARK: - Properties
    private var captureSession: AVCaptureSession!
    private var previewLayer: AVCaptureVideoPreviewLayer!
    
    var delegate: ScannerViewControllerProtocol?

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCamera()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopScanning()
    }
    
    // MARK: - Helpers
    func setupCamera() {
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        let cornerCode = UIImageView(image: UIImage(systemName: "camera.metering.center.weighted.average"))
        cornerCode.tintColor = colorRnt
        
        view.addSubview(cornerCode.withSize(.init(width: 360, height: 360)))
        cornerCode.centerInSuperview()
        
        let closeButton = UIButton(image: (UIImage(systemName: "xmark")?.resizeImage(size: .init(width: 30, height: 30)))!, target: self, action: #selector(closeScannerViewControllerSelector))
        closeButton.tintColor = .white
        closeButton.backgroundColor = .systemRed
        closeButton.roundedButton(cornerRadius: 20, color: .systemRed)
        
        view.addSubview(closeButton.withSize(.init(width: 40, height: 40)))
        closeButton.anchor(top: view.topAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor, padding: .allSides(16))
        
        
    }

    func startScanning() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.captureSession.startRunning()
        }
    }

    func stopScanning() {
        captureSession.stopRunning()
        self.dismiss(animated: true)
    }

    func failed() {
        let alert = UIAlertController(title: "Error", message: "No se pudo iniciar la cámara", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
        captureSession = nil
    }
    
    func found(code: String) {
        print("code: ", code)
        self.delegate?.infoRecoveredFromScanner(info: code, camera: true)
        stopScanning()
    }
    
    func processImage(_ image: UIImage) {
        guard let ciImage = CIImage(image: image) else {
            print("Error al convertir la imagen a CIImage")
            return
        }
        
        let context = CIContext()
        let options: [String: Any] = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let detector = CIDetector(ofType: CIDetectorTypeQRCode, context: context, options: options)
        
        let features = detector?.features(in: ciImage)
        
        if let firstFeature = features?.first as? CIQRCodeFeature {
            let scannedValue = firstFeature.messageString ?? "No se pudo obtener el valor escaneado"
            print("Código QR escaneado: \(scannedValue)")
            self.delegate?.infoRecoveredFromScanner(info: scannedValue, camera: false)
            // Aquí puedes realizar acciones adicionales con el valor escaneado
            
            captureSession.stopRunning()
        } else {
            self.delegate?.infoRecoveredFromScanner(info: "No se encontraron códigos QR en la imagen", camera: false)
            print("No se encontraron códigos QR en la imagen")
        }
    }
    
    //MARK: - Selectors
    @objc private func closeScannerViewControllerSelector(){
        stopScanning()
    }
}

//MARK: - AVCaptureMetadataOutputObjectsDelegate
extension ScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    //Detecta un objeto de metadatos: código QR
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        stopScanning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
    }
}
