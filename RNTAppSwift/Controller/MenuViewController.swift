//
//  MenuViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 21/11/23.
//

import UIKit
import SideMenu


// MARK: - Protocol MenuProtocol
protocol MenuViewProtocol {
    func menuGoToController(destination: String)
}

class MenuViewController: UIViewController {
    // MARK: - Properties
    private let menuReuseIdentifier = "menuReuseIdentifier"
    private lazy var logoutButton = UIButton(title: Constants.logOutText, titleColor: .white, font: .montserratBold(ofSize: 16), backgroundColor: colorRnt!, target: self, action: #selector(logoutSelector))
    private lazy var loginButton = UIButton(title: Constants.loginText , titleColor: .white, font: .montserratBold(ofSize: 16), backgroundColor: colorDarkYellow!, target: self, action: #selector(loginSelector))
    private lazy var siginButton = UIButton(title: Constants.signInText, titleColor: colorGrayTextButton!, font: .montserratBold(ofSize: 16), backgroundColor: .clear, target: self, action: #selector(siginSelector))
    private let imageView = UIImageView(image: nil, contentMode: .scaleAspectFit)
    
    private lazy var closeMenuButton = UIButton(image: UIImage(systemName: "xmark")!, target: self, action: #selector(closeMenuSelector))
    
    private lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.register(MenuTableViewCell.self, forCellReuseIdentifier: menuReuseIdentifier)
        tv.dataSource = self
        tv.delegate = self
        tv.backgroundColor = colorRnt
        tv.tableFooterView = UIView()
        tv.separatorColor = .lightGray
        return tv
    }()
    
    private let versionLabel = UILabel(text: "", font: .montserratBold(ofSize: 12), textColor: .lightGray, textAlignment: .center)
    
    private var mx360ImageView =  UIImageView(image: UIImage(named: "mxImage"), contentMode: .scaleAspectFit)
    private var tourisImageView = UIImageView(image: UIImage(named: "LaunchS"), contentMode: .scaleAspectFit)
    private lazy var bottonView: UIView = {
        let v = UIView(backgroundColor: colorView!)
        
        v.addSubview(mx360ImageView.withSize(.init(width: 120, height: 35)))
        v.addSubview(tourisImageView.withSize(.init(width: 120, height: 35)))
        
        tourisImageView.anchor(top: v.topAnchor, leading: v.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 16, left: 16, bottom: 0, right: 8))
        mx360ImageView.anchor(top: v.topAnchor, leading: nil, bottom: nil, trailing: v.trailingAnchor, padding: .init(top: 16, left: 8, bottom: 8, right: 16))
        
        return v
    }()
    
    
    var delegate: MenuViewProtocol?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false)
        setupViewComponents()
        versionLabel.text = Constants.versionWithPointsText + GeneralFunctions.shareManager.getVersionApp()
    }
    
    // MARK: - Helpers
    fileprivate func setupViewComponents() {
        view.backgroundColor = colorRnt
        logoutButton.tintColor = .white
        logoutButton.roundedButton(cornerRadius: 5, color: .white)
        loginButton.roundedButton(cornerRadius: 5, color: colorDarkYellow!)
        siginButton.roundedButton(cornerRadius: 5, color: colorGrayTextButton!)
        
        view.addSubview(logoutButton)
        view.addSubview(loginButton)
        view.addSubview(siginButton)
        view.addSubview(versionLabel)
        view.addSubview(bottonView.withHeight(70))
        
        bottonView.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        versionLabel.anchor(top: nil, leading: view.leadingAnchor, bottom: bottonView.topAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 16, right: 0))
        
        siginButton.anchor(top: nil, leading: nil, bottom: versionLabel.topAnchor, trailing: nil, padding: .init(top: 0, left: 0, bottom: 16, right: 0), size: .init(width: 250, height: 40))
        siginButton.centerXToSuperview()
        
        loginButton.anchor(top: nil, leading: nil, bottom: siginButton.topAnchor, trailing: nil, padding: .init(top: 0, left: 0, bottom: 16, right: 0), size: .init(width: 250, height: 40))
        loginButton.centerXToSuperview()
        
        logoutButton.anchor(top: nil, leading: nil, bottom: loginButton.topAnchor, trailing: nil, padding: .init(top: 0, left: 0, bottom: 16, right: 0), size: .init(width: 250, height: 40))
        logoutButton.centerXToSuperview()
        
        view.addSubview(tableView)
        tableView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: logoutButton.topAnchor, trailing: view.trailingAnchor, padding: .init(top: 50, left: 0, bottom: 0, right: 0))
        view.addSubview(closeMenuButton.withSize(.init(width: 30, height: 30)))
        closeMenuButton.anchor(top: nil, leading: nil, bottom: tableView.topAnchor, trailing: view.trailingAnchor, padding: .allSides(16))
        closeMenuButton.imageView?.fillSuperview()
        closeMenuButton.tintColor = .white
    }
    
    // MARK: - Selectors
    @objc private func logoutSelector() {
        self.delegate?.menuGoToController(destination: "-1")
    }
    
    @objc private func loginSelector() {
        self.delegate?.menuGoToController(destination: "-2")
    }
    
    @objc private func siginSelector() {
        self.delegate?.menuGoToController(destination: "-3")
    }
    
    
    @objc private func closeMenuSelector(){
        self.dismiss(animated: true)
    }
}

// MARK: - UITableViewDataSource
extension MenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuOptions.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: menuReuseIdentifier, for: indexPath) as! MenuTableViewCell
        cell.setupDataCell(MenuOptions(rawValue: indexPath.row)!)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let options = indexPath.row
        self.delegate?.menuGoToController(destination: "\(options)")
        self.dismiss(animated: true)
    }
}

