//
//  FullScreenImagesViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 05/12/23.
//

import UIKit

class FullScreenImagesViewController: UIViewController {
    //MARK: - Properties
    private lazy var pageViewController: UIPageViewController = {
        let pvc = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pvc.dataSource = self
        pvc.delegate = self
        return pvc
    }()
    private var pageControl = UIPageControl()
    
    private lazy var cancelButton = UIButton(image: (UIImage(systemName: "xmark")?.resizeImage(size: .init(width: 25, height: 25)))! , target: self, action: #selector(closeSelector))
    
    var closeHandlerFullScreen: (() -> Void)?
    
    private var images: [String] = []
    private var currentIndex: Int = 0
    
    //MARK: - Life Cycle
    init(images: [String], initialIndex: Int = 0) {
        super.init(nibName: nil, bundle: nil)
        self.images = images
        self.currentIndex = initialIndex
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colorBackground
        setupViewComponents()
    }
    
    //MARK: - Helpers
    private func setupViewComponents() {
        if let initialViewController = imageViewController(at: currentIndex) {
            pageViewController.setViewControllers([initialViewController], direction: .forward, animated: false, completion: nil)
        }
        
        addChild(pageViewController)
        view.addSubview(pageViewController.view)
        pageViewController.didMove(toParent: self)
        
        view.addSubview(pageControl)
        view.addSubview(cancelButton.withSize(.init(width: 40, height: 40)))
        
        pageControl.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .allSides(20))
        pageControl.numberOfPages = images.count
        pageControl.currentPage = currentIndex
        pageControl.currentPageIndicatorTintColor = colorRnt
        pageControl.pageIndicatorTintColor = .darkGray
        pageControl.addTarget(self, action: #selector(pageControlChanged), for: .valueChanged)
        pageControl.centerYTo(cancelButton.centerYAnchor)
        
        cancelButton.anchor(top: view.topAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor, padding: .allSides(20))
        cancelButton.backgroundColor = .red
        cancelButton.tintColor = .white
        cancelButton.roundedCorners(cornerRadius: 20, borderWith: 1, borderColor: .red)
    }
    
    private func imageViewController(at index: Int) -> UIViewController? {
        guard index >= 0, index < images.count else {
            return nil
        }
        
        let imageVC = ImageViewController(image: images[index])
        return imageVC
    }
    
    //MARK: - Selectors
    @objc private func closeSelector(){
        closeHandlerFullScreen?()
    }
    
    @objc private func pageControlChanged() {
        if let currentVC = pageViewController.viewControllers?.first as? ImageViewController,
           var currentIndex = images.firstIndex(of: currentVC.image!) {
            let newIndex = pageControl.currentPage
            
            let direction: UIPageViewController.NavigationDirection = newIndex > currentIndex ? .forward : .reverse
            currentIndex = newIndex
            pageViewController.setViewControllers([imageViewController(at: currentIndex)!], direction: direction, animated: true, completion: nil)
        }
    }
}

//MARK: - UIPageViewControllerDataSource
extension FullScreenImagesViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentVC = viewController as? ImageViewController, let currentIndex = images.firstIndex(of: currentVC.image!), currentIndex > 0 else { return nil }
        return imageViewController(at: currentIndex - 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentVC = viewController as? ImageViewController, let currentIndex = images.firstIndex(of: currentVC.image!), currentIndex < images.count - 1 else { return nil }
        return imageViewController(at: currentIndex + 1)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return images.count
    }
}
//MARK: - UIPageViewControllerDelegate
extension FullScreenImagesViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let currentVC = pageViewController.viewControllers?.first as? ImageViewController,
           let currentIndex = images.firstIndex(of: currentVC.image!) {
            pageControl.currentPage = currentIndex
        }
    }
}
