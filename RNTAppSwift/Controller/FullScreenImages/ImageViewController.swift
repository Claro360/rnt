//
//  ImageViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 05/12/23.
//

import UIKit

class ImageViewController: UIViewController {
    //MARK: - Properties
    private let imageView = UIImageView()
    var image: String?

    //MARK: - Life Cycle
    init(image: String) {
        super.init(nibName: nil, bundle: nil)
        self.image = image
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupImageView()
    }

    //MARK: - Helpers
    private func setupImageView() {
        imageView.sd_setImage(with: URL(string: image ?? "")){ [weak self] (image, error, cacheType, url)  in
            if error != nil {
                self?.imageView.image = UIImage(named: "LaunchS")
            }
        }
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(imageView)
        imageView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
    }
}
