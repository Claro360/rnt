//
//  FavoritesViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 30/11/23.
//

import UIKit
import SideMenu

class FavoritesViewController: UIViewController {
    // MARK: - Properties
    let navBar = CustomNavBarView(tittle: Constants.myFavoritesText, withBottonLine: 1)
    
    lazy var refreshController: UIRefreshControl = {
        let r = UIRefreshControl()
        r.addTarget(self, action: #selector(refreshFavColectionView), for: .valueChanged)
        return r
    }()
    
    let favIdCell = "favIdCell"
    let clearIdCell = "clearIdCell"
    lazy var favColectionView: UICollectionView = {
        let l = UICollectionViewFlowLayout()
        l.scrollDirection = .vertical
        //l.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: l)
        cv.dataSource = self
        cv.delegate = self
        cv.register(FavoritesCollectionViewCell.self, forCellWithReuseIdentifier: favIdCell)
        cv.register(ClearFavoritesCollectionViewCell.self, forCellWithReuseIdentifier: clearIdCell)
        cv.backgroundColor = colorBackground
        cv.contentInset = .init(top: 0, left: 16, bottom: 0, right: 16)
        cv.showsHorizontalScrollIndicator = false
        cv.refreshControl = refreshController
        return cv
    }()
    
    var model: [modelFavorites] = []
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colorBackground
        model = modelAux
        setupViewComponents()
    }

    // MARK: - Helpers
    private func setupViewComponents(){
        view.addSubview(navBar.withHeight(100))
        navBar.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
        
        view.addSubview(favColectionView)
        favColectionView.anchor(top: navBar.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 20, left: 0, bottom: 20, right: 0))
        
        navBar.buttonTappedHandler = { [weak self] option in
            if option == "back" {
                guard let viewControllers = self?.navigationController?.viewControllers else { return }
                for firstViewController in viewControllers {
                    if firstViewController is SelectionForLoginViewController {
                        self?.navigationController?.popToViewController(firstViewController, animated: true)
                        break
                    }
                }
            }else{
                var menu: SideMenuNavigationController {
                    let vc = MenuViewController()
                    vc.delegate = self
                    let menu = SideMenuNavigationController(rootViewController: vc)
                    menu.settings.menuWidth = (UIScreen.main.bounds.width / 5) * 4
                    menu.presentationStyle = .menuSlideIn
                    menu.statusBarEndAlpha = 0
                    menu.presentationStyle.onTopShadowColor = .lightGray
                    menu.presentationStyle.onTopShadowRadius = 5
                    menu.presentationStyle.onTopShadowOpacity = 0.5
                    menu.presentationStyle.onTopShadowOffset = .zero
                    return menu
                }
                self?.present(menu, animated: true)
            }
        }
    }
    
    // MARK: - Selectors
    @objc private func refreshFavColectionView(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            modelAux.removeAll()
            modelAux = self.model
            self.favColectionView.reloadData()
            self.refreshController.endRefreshing()
        }
    }

}
//MARK: - UITableViewDataSource
extension FavoritesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if modelAux.count == 0{
            return 1
        }else{
            return modelAux.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if modelAux.count == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: clearIdCell, for: indexPath) as! ClearFavoritesCollectionViewCell
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: favIdCell, for: indexPath) as! FavoritesCollectionViewCell
            let data = modelAux[indexPath.row]
            cell.setupDataCell(data)
            cell.roundedView(cornerRadius: 10, color: .clear)
            cell.addShadow()
            cell.handlerCellFavorits = { [weak self] (option, position) in
                if option == "F" {
                    let actionDone = UIAlertAction(title: "Eliminar", style: .destructive) { _ in
                        modelAux.remove(at: indexPath.row)
                        self?.favColectionView.reloadData()
                    }
                    let actionCancel = UIAlertAction(title: "Cancelar", style: .default)
                    GeneralFunctions.shareManager.showAlert(title: "Favoritos", message: "Se eliminara este Prestador de Servicios Turisticos de tus favoritos", actionDone: actionDone, actionCancel: actionCancel, context: self!)
                }else if option == "Imgs" {
                    let vc = FullScreenImagesViewController(images: data.images!, initialIndex: position)
                    self!.present(vc, animated: true)
                    vc.closeHandlerFullScreen = {
                        vc.dismiss(animated: true)
                    }
                }else{
                    GeneralFunctions.shareManager.openApp(AppName: option)
                }
            }
            return cell
        }
    }
    
}
//MARK: - UITableViewDelegate
extension FavoritesViewController: UICollectionViewDelegate { }
extension FavoritesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.frame.width - 32, height: 300)
    }
}


//MARK: - MenuViewProtocol
extension FavoritesViewController: MenuViewProtocol{
    func menuGoToController(destination: String) {
        GeneralFunctions.shareManager.gotToController(nombreControlador: destination, contexto: self)
    }
}


var modelAux: [modelFavorites] = [modelFavorites(images: ["https://rnt.mexico360app.com/resources/Img/turismo.png", "https://lineamientos.s3.amazonaws.com/attachmentsChats/9991336774_to_9991336781/8_mexico360.png", "https://lh3.googleusercontent.com/ogw/AKPQZvwYfEkPsAUmbEnwseLgzicanBes_mp2x1U_4dKGjw=s64-c-mo", "https://images.app.goo.gl/V7wyLK9o8wdUX4wb7"], name: "Turismo Sendetur", type: "AGENCIA DE VIAJES", direction: "Insurgentees Centro 137, San Rafael, Cuautémoc, Ciudad de México", buttonsContct: ["icono_correo_electronico", "icono_facebook", "icono_sitio_web", "icono_telefono", "icono_tiktok", "icono_whatsapp", "icono_x"], favorit: true),
                                   modelFavorites(images: ["https://previews.123rf.com/images/shushanto/shushanto2209/shushanto220900703/191842443-imagen-de-fondo-de-la-ilustraci%C3%B3n-del-arte-conceptual-de-la-destrucci%C3%B3n-de-los-planetas.jpg", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcScwL7vqfUE_pq6ZEvpIpIG_QBTgaNxaMlvLw&usqp=CAU", "https://bucketmoviles121652-dev.s3.amazonaws.com/public/MobileCard/notFound.png", "https://w7.pngwing.com/pngs/786/98/png-transparent-viva-mexico-mexican-sahuaro-tequila-hat-music-guitar-man-voice-tourism-thumbnail.png"], name: "Karr-Nitas", type: "ALIMENTOS Y BEBIDAS", direction: "Granjas San Francisco, Hipódromo, Azcapotzalco, Ciudad de México", buttonsContct: ["icono_correo_electronico", "icono_facebook", "icono_sitio_web", "icono_telefono", /*"icono_tiktok", */"icono_whatsapp", "icono_x"], favorit: true),
                                   modelFavorites(images: ["https://qph.cf2.quoracdn.net/main-qimg-e6c981932d7c0c167922d77e52a3aa5a-lq", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTjge4_5073zvwb4X2q7ap4M-MAkbhNSGCkFw&usqp=CAU", "https://d500.epimg.net/cincodias/imagenes/2019/11/04/lifestyle/1572892359_005767_1572892909_noticia_normal.jpg", "https://ichef.bbci.co.uk/news/640/cpsprodpb/4fef/live/26f28080-6ad1-11ee-8073-5b93bd1aa7db.jpg"], name: "Turismo ___", type: "BAR RESTAURANTE", direction: "Insurgentees Centro 137, San Rafael, Cuautémoc, Ciudad de México", buttonsContct: ["icono_correo_electronico", "icono_facebook", /*"icono_sitio_web", "icono_telefono", "icono_tiktok", */"icono_whatsapp", "icono_x"], favorit: true)]
