//
//  WebViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 24/11/23.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    // MARK: - properties
    lazy var webViewController: WKWebView = {
        let webV = WKWebView(frame: .zero)
        webV.navigationDelegate = self
        return webV
    }()
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewComponents()
    }
    
    // MARK: - Helpers
    private func setupViewComponents(){
        DispatchQueue.main.async { [weak self] in
            self?.webViewController.load(NSURLRequest(url: NSURL(string:"https://rnt.mexico360app.com/AppTurista")! as URL) as URLRequest)
        }
        
        view.addSubview(webViewController)
        webViewController.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        webViewController.centerXTo(view.centerXAnchor)
    }

}


//MARK: WKUIDelegate
extension WebViewController: WKUIDelegate {
    @available(iOS 15.0, *)
    func webView(_ webView: WKWebView, decideMediaCapturePermissionsFor origin: WKSecurityOrigin, initiatedBy frame: WKFrameInfo, type: WKMediaCaptureType) async -> WKPermissionDecision {
        return .grant;
    }
}
//MARK: WKNavigationDelegate
extension WebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Llega aqui cuando la página del WebView termina de cargarse")
    }
}
