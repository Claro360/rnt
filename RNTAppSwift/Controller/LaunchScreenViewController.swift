//
//  LaunchScreenViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 15/11/23.
//

import UIKit
import LBTATools
import CoreLocation

class LaunchScreenViewController: UIViewController {
    // MARK: - Properties
    private var locationManager = CLLocationManager()
    private var launcgImageView = UIImageView(image: UIImage.gifImageWithName("RNT-Sin_fondo"), contentMode: .scaleAspectFit)

    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = colorBackground
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setupLocationManager()
        view.addSubview(launcgImageView)
        launcgImageView.centerInSuperview(size: .init(width: 360, height: 360))
    }
    
    override func viewDidLayoutSubviews() {
        gotToSecondController()
    }
    
    // MARK: - Helpers
    private func gotToSecondController(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
                let window = windowScene.windows.filter({ $0.isKeyWindow }).first {
                //window.rootViewController = UINavigationController(rootViewController: WebViewController())
                window.rootViewController = UINavigationController(rootViewController: HomeViewController())
                //window.rootViewController = UINavigationController(rootViewController: FavoritesViewController())
            }
        })
    }
    
    private func setupLocationManager(){
        locationManager.requestWhenInUseAuthorization()
        switch locationManager.authorizationStatus {
            case .notDetermined, .restricted, .denied:
                locationManager.requestWhenInUseAuthorization()
                let alert = UIAlertController(title: "Aviso", message: "Es necesario el permiso de ubicación. Configuración -> Privacidad -> Localización", preferredStyle: .alert)
                let action = UIAlertAction(title: "Aceptar", style: .default,handler: { action in
                    GeneralFunctions.shareManager.actionPermisosLocation()
                })
                alert.addAction(action)
                self.present(alert, animated: true)
                
            case .authorizedWhenInUse, .authorizedAlways:
                print("Autoizado")
            default:
                break
        }
    }
}
