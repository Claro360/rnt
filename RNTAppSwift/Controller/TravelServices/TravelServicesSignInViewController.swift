//
//  TravelServicesSignInViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 21/11/23.
//

import UIKit
import SideMenu
import LBTATools
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class TravelServicesSignInViewController: LBTAFormController {
    // MARK: - Properties
    private let navBar = CustomNavBarView(tittle: Constants.recordText)
    
    private let sscrolView = UIScrollView()
    
    private let tittleLabel = UILabel(text: Constants.registrationProcessText, font: .montserratBold(ofSize: 22), textColor: colorGrayTextLabel!, textAlignment: .center)
    private let lineBrownView = UIView(backgroundColor: colorBrownRnt!)
    private let rectangleBrownView = UIView(backgroundColor: colorBrownRnt!)
    
    private let subTittleLabel = UILabel(text: Constants.registerOnRntText, font: .montserratRegular(ofSize: 14), textColor: colorGrayTextLabel!, numberOfLines: 0)
    
    private let tradenameLabel = UILabel(text: Constants.tradenameWithPointsText, font: .montserratBold(ofSize: 14), textColor: colorGrayTextLabel!)
    private let tradenameTextField = MDCOutlinedTextField()
    
    private let typePSTLabel = UILabel(text: Constants.typePSTWithPointsText, font: .montserratBold(ofSize: 14), textColor: colorGrayTextLabel!)
    private var typePstTextField = MDCOutlinedTextField()
    private lazy var typePSTButton = UIButton(title: "", font: .montserratRegular(ofSize: 14), target: self, action: #selector(typePSTSelector))
    
    private let categoryLabel = UILabel(text: Constants.categoryWithPointsText, font: .montserratBold(ofSize: 14), textColor: colorGrayTextLabel!)
    private var categoryTextField = MDCOutlinedTextField()
    private lazy var categoryButton = UIButton(title: "", font: .montserratRegular(ofSize: 14), target: self, action: #selector(categorySelector))
    
    private let namesLabel = UILabel(text: Constants.namesWithPointsText + "*", font: .montserratBold(ofSize: 14), textColor: colorGrayTextLabel!)
    private let namesTextField = MDCOutlinedTextField()
    
    private let firstSurnameLabel = UILabel(text: Constants.firstSurnameWithPointsText + "*", font: .montserratBold(ofSize: 14), textColor: colorGrayTextLabel!)
    private let firstSurnameTextField = MDCOutlinedTextField()
    
    private let secondSurnameLabel = UILabel(text: Constants.secondSurnameWithPointsText + "*", font: .montserratBold(ofSize: 14), textColor: colorGrayTextLabel!)
    private let secondSurnameTextField = MDCOutlinedTextField()
    
    private let emailLabel = UILabel(text: Constants.emailWithPointsText + "*", font: .montserratBold(ofSize: 14), textColor: colorGrayTextLabel!)
    private let emailTextField = MDCOutlinedTextField()
    
    private let passLabel = UILabel(text: Constants.passwordWithPointsText, font: .montserratBold(ofSize: 14), textColor: colorGrayTextLabel!)
    private let passTextField = MDCOutlinedTextField()
    
    private let confirmPassLabel = UILabel(text: Constants.confirmPasswordWithPointsText, font: .montserratBold(ofSize: 14), textColor: colorGrayTextLabel!)
    private let confirmPassTextField = MDCOutlinedTextField()
    
    private let requiredFieldsLabel = UILabel(text: Constants.requiredFieldsText, font: .montserratRegular(ofSize: 12), textColor: colorGrayTextLabel!, textAlignment: .left)
    
    private lazy var sigInButton = UIButton(title: Constants.signIn_LabelText, titleColor: colorRnt!, font: .montserratBold(ofSize: 15), backgroundColor: .clear, target: self, action: #selector(toRequestViewController))
    
    private lazy var logingWithExistingAccountButton = UIButton(title: Constants.logingWithExistingAccountText, titleColor: .link, backgroundColor: .clear, target: self, action: #selector(toLoginSelector))
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = colorBackground
        setupViewComponents()
    }
    

    // MARK: - Helpers
    private func setupViewComponents(){
        view.addSubview(navBar.withHeight(100))
        navBar.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
        
        sigInButton.roundedButton(cornerRadius: 5, color: colorRnt!, borderWidth: 2)
        logingWithExistingAccountButton.textUnderLined()
        
        tradenameTextField.setupMDCOutlinedTextField(placeholder: Constants.tradenameText, font: .montserratRegular(ofSize: 12))
        tradenameTextField.keyboardType = UIKeyboardType.asciiCapable
        typePstTextField.setupMDCOutlinedTextField(placeholder: Constants.selectOptionText, font: .montserratRegular(ofSize: 12))
        typePstTextField.rightViewMode = UITextField.ViewMode.always
        let ImageViewDown = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        ImageViewDown.image = UIImage(systemName: "chevron.down")?.withTintColor(.lightGray)
        ImageViewDown.tintColor = .lightGray
        typePstTextField.rightView = ImageViewDown
        
        categoryTextField.setupMDCOutlinedTextField(placeholder: Constants.selectOptionText, font: .montserratRegular(ofSize: 12))
        categoryTextField.keyboardType = UIKeyboardType.asciiCapable
        categoryTextField.rightViewMode = UITextField.ViewMode.always
        let ImageViewDown1 = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 10))
        ImageViewDown1.image = UIImage(systemName: "chevron.down")?.withTintColor(.lightGray)
        ImageViewDown1.tintColor = .lightGray
        categoryTextField.rightView = ImageViewDown1
        
        namesTextField.setupMDCOutlinedTextField(placeholder: Constants.namesText, font: .montserratRegular(ofSize: 12))
        namesTextField.keyboardType = UIKeyboardType.asciiCapable
        firstSurnameTextField.setupMDCOutlinedTextField(placeholder: Constants.firstSurnameText, font: .montserratRegular(ofSize: 12))
        firstSurnameTextField.keyboardType = UIKeyboardType.asciiCapable
        secondSurnameTextField.setupMDCOutlinedTextField(placeholder: Constants.secondSurnameText, font: .montserratRegular(ofSize: 12))
        secondSurnameTextField.keyboardType = UIKeyboardType.asciiCapable
        emailTextField.setupMDCOutlinedTextField(placeholder: Constants.emailText, font: .montserratRegular(ofSize: 12))
        emailTextField.keyboardType = UIKeyboardType.asciiCapable
        passTextField.setupMDCOutlinedTextField(placeholder: Constants.passwordText, font: .montserratRegular(ofSize: 12))
        passTextField.keyboardType = UIKeyboardType.asciiCapable
        passTextField.trailingView = passTextField.addButtonHidenPass()
        confirmPassTextField.setupMDCOutlinedTextField(placeholder: Constants.confirmPasswordText, font: .montserratRegular(ofSize: 12))
        confirmPassTextField.keyboardType = UIKeyboardType.asciiCapable
        confirmPassTextField.trailingView = confirmPassTextField.addButtonHidenPass()
        
        
        let contairnerView = UIView()
        contairnerView.stack(contairnerView.stack(tittleLabel.withWidth(Size.width + 24),
                                                  lineBrownView.withSize(.init(width: Size.width + 32, height: 1)),
                                                  rectangleBrownView.withSize(.init(width: 40, height: 5)), spacing: 0, alignment: .leading),
                             subTittleLabel.withWidth(Size.width + 24),
                             contairnerView.stack(tradenameLabel,
                                                  tradenameTextField.withWidth(Size.width + 24)),
                             contairnerView.stack(typePSTLabel,
                                                  typePstTextField.withWidth(Size.width + 24)),
                             contairnerView.stack(categoryLabel,
                                                  categoryTextField.withWidth(Size.width + 24)),
                             contairnerView.stack(namesLabel,
                                                  namesTextField.withWidth(Size.width + 24)),
                             contairnerView.stack(firstSurnameLabel,
                                                  firstSurnameTextField.withWidth(Size.width + 24)),
                             contairnerView.stack(secondSurnameLabel,
                                                  secondSurnameTextField.withWidth(Size.width + 24)),
                             contairnerView.stack(emailLabel,
                                                  emailTextField.withWidth(Size.width + 24)),
                             contairnerView.stack(passLabel,
                                                  passTextField.withWidth(Size.width + 24)),
                             contairnerView.stack(confirmPassLabel,
                                                  confirmPassTextField.withWidth(Size.width + 24)),
                             requiredFieldsLabel.withWidth(Size.width + 24),
                             sigInButton.withSize(.init(width: CGFloat(sigInButton.currentTitle!.count * 16), height: 40)),
                             logingWithExistingAccountButton,
                             spacing: 20,
                             alignment: .center)
        
        view.addSubview(sscrolView)
        sscrolView.anchor(top: navBar.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0))
        
        sscrolView.addSubview(contairnerView)
        contairnerView.anchor(top: sscrolView.topAnchor, leading: sscrolView.leadingAnchor, bottom: sscrolView.bottomAnchor, trailing: sscrolView.trailingAnchor)
        contairnerView.centerXTo(sscrolView.centerXAnchor)
        
        sscrolView.addSubview(typePSTButton)
        sscrolView.addSubview(categoryButton)
        
        typePSTButton.anchor(top: typePstTextField.topAnchor, leading: typePstTextField.leadingAnchor, bottom: typePstTextField.bottomAnchor, trailing: typePstTextField.trailingAnchor)
        
        categoryButton.anchor(top: categoryTextField.topAnchor, leading: categoryTextField.leadingAnchor, bottom: categoryTextField.bottomAnchor, trailing: categoryTextField.trailingAnchor)
        
        
        navBar.buttonTappedHandler = { [weak self] type in
            if type == "back" {
                guard let viewControllers = self?.navigationController?.viewControllers else { return }
                for firstViewController in viewControllers {
                    if firstViewController is SelectionForLoginViewController {
                        self?.navigationController?.popToViewController(firstViewController, animated: true)
                        break
                    }
                }
            }else{
                var menu: SideMenuNavigationController {
                    let vc = MenuViewController()
                    vc.delegate = self
                    let menu = SideMenuNavigationController(rootViewController: vc)
                    menu.settings.menuWidth = (UIScreen.main.bounds.width / 5) * 4
                    menu.presentationStyle = .menuSlideIn
                    menu.statusBarEndAlpha = 0
                    menu.presentationStyle.onTopShadowColor = .lightGray
                    menu.presentationStyle.onTopShadowRadius = 5
                    menu.presentationStyle.onTopShadowOpacity = 0.5
                    menu.presentationStyle.onTopShadowOffset = .zero
                    return menu
                }
                self?.present(menu, animated: true)
            }
        }
    }
    
    
    // MARK: - Selectors
    @objc private func typePSTSelector(_ sender: UIButton) {
        sender.setTitle(Constants.selectOptionText, for: .normal)
        sender.titleLabel?.font = .montserratRegular(ofSize: 12)
        GeneralFunctions.shareManager.pickerDataSelection(tittle: Constants.typeOfPSTText, rows: ["Type Pst 1", "Type Pst 2", "Type Pst 3", "Type Pst 4"], sender: sender)
        typePstTextField.placeholder = ""
        
        sender.contentHorizontalAlignment = .leading
        sender.setTitleColor(colorGrayTextPlaceholder!, for: .normal)
    }

    @objc private func categorySelector(_ sender: UIButton) {
        sender.setTitle(Constants.selectOptionText, for: .normal)
        sender.titleLabel?.font = .montserratRegular(ofSize: 12)
        GeneralFunctions.shareManager.pickerDataSelection(tittle: Constants.categoriesText, rows: ["Categoria 1", "Categoria 2", "Categoria 3", "Categoria 4"], sender: sender)
        categoryTextField.placeholder = ""
        sender.contentHorizontalAlignment = .left
        sender.setTitleColor(colorGrayTextPlaceholder!, for: .normal)
    }
    
    @objc private func toLoginSelector(){
        if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
           let window = windowScene.windows.filter({ $0.isKeyWindow }).first {
            window.rootViewController = UINavigationController(rootViewController: LoginViewController())
        }
    }
    
    @objc private func toRequestViewController(){
        navigationController?.pushViewController(RequestsViewController(), animated: true)
    }
}

extension TravelServicesSignInViewController: MenuViewProtocol{
    func menuGoToController(destination: String) {
        GeneralFunctions.shareManager.gotToController(nombreControlador: destination, contexto: self)
    }
}
