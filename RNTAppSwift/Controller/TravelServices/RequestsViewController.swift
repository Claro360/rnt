//
//  RequestsViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 22/11/23.
//

import UIKit
import SideMenu
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class RequestsViewController: UIViewController{
    // MARK: - Properties
    private let navBar = CustomNavBarView(tittle: Constants.requestRntText)
    private let sscrolView = UIScrollView()
    
    private let tittleLabel = UILabel(text: Constants.requestRntText, font: .montserratBold(ofSize: 22), textColor: colorGrayTextLabel!, textAlignment: .center)
    private let lineBrownView = UIView(backgroundColor: colorBrownRnt!)
    private let rectangleBrownView = UIView(backgroundColor: colorBrownRnt!)
    
    ///View no requests
    private let startRequestLabel = UILabel(text: Constants.startRequestText, font: .montserratBold(ofSize: 18), textColor: colorGrayTextLabel!, textAlignment: .center)
    private lazy var newRequestButton = UIButton(title: Constants.newRequestText, titleColor: .white, font: .montserratBold(ofSize: 15), backgroundColor: colorRnt!, target: self, action: #selector(self.toRegistrationApplication))
    
    private lazy var viewClear: UIView = {
        let v = UIView(backgroundColor: .clear)
        v.layer.borderWidth = 1
        v.layer.borderColor = UIColor.lightGray.cgColor
        
        v.addSubview(startRequestLabel)
        startRequestLabel.centerInSuperview()
        return v
    }()
    
    ///View with requests
    var requests: [requestsModel] = [requestsModel(requestNumber: "04090150001", tradename: "Turismo", applicant: "Nombre demo", pst: "Agencia de Viajes"),
                    requestsModel(requestNumber: "04090150002", tradename: "Turismo", applicant: "Nombre dem 2", pst: "Agencia de Viajes"),
                    requestsModel(requestNumber: "04090150003", tradename: "Turism 2", applicant: "Nombre dem 3", pst: "Agencia de Viajes")]
    
    private let toFilterLabel = UILabel(text: Constants.toFilterText, font: .montserratBold(ofSize: 10), textColor: colorGrayTextLabel!, textAlignment: .center)
    private let filterRequestTextField = MDCOutlinedTextField()
    private lazy var searchView: UIView = {
        let v = UIView()
        v.addSubview(toFilterLabel.withWidth(Double(toFilterLabel.text!.count * 5)))
        v.addSubview(filterRequestTextField)
        
        toFilterLabel.anchor(top: v.topAnchor, leading: v.leadingAnchor, bottom: v.bottomAnchor, trailing: filterRequestTextField.leadingAnchor, padding: .init(top: 8, left: 0, bottom: 8, right: 8))
        filterRequestTextField.anchor(top: v.topAnchor, leading: toFilterLabel.trailingAnchor, bottom: v.bottomAnchor, trailing: v.trailingAnchor, padding: .init(top: 8, left: 8, bottom: 8, right: 0))
        
        return v
    }()
    
    let identifierCell = "identifierCell"
    let headerCell = "headerCell"
    lazy var requestTableView: UITableView = {
        let tb = UITableView()
        tb.register(RequestTableViewCell.self, forCellReuseIdentifier: identifierCell)
        tb.register(HeaderRequestCollectionReusableView.self, forHeaderFooterViewReuseIdentifier: headerCell)
        tb.showsVerticalScrollIndicator = true
        tb.backgroundColor = colorBackground
        tb.delegate = self
        tb.dataSource = self
        return tb
    }()
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        requests.removeAll()
        view.backgroundColor = colorBackground
        setupViewNavBar()
        if requests.count == 0 {
            setupViewComponentsWithOutRequests()
        }else{
            setupViewComponentsWithRequests()
        }
        
        
    }
    
    // MARK: - Helpers
    private func setupViewNavBar() {
        filterRequestTextField.setupMDCOutlinedTextField(placeholder: Constants.searchText, font: .montserratRegular(ofSize: 10))
        newRequestButton.roundedButton(cornerRadius: 5, color: colorRnt!)
        
        view.addSubview(navBar.withHeight(100))
        navBar.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
        
        view.addSubview(sscrolView)
        sscrolView.anchor(top: navBar.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0))
        
        navBar.buttonTappedHandler = { [weak self] type in
            if type == "back" {
                guard let viewControllers = self?.navigationController?.viewControllers else { return }
                for firstViewController in viewControllers {
                    if firstViewController is SelectionForLoginViewController {
                        self?.navigationController?.popToViewController(firstViewController, animated: true)
                        break
                    }
                }
            }else{
                var menu: SideMenuNavigationController {
                    let vc = MenuViewController()
                    vc.delegate = self
                    let menu = SideMenuNavigationController(rootViewController: vc)
                    menu.settings.menuWidth = (UIScreen.main.bounds.width / 5) * 4
                    menu.presentationStyle = .menuSlideIn
                    menu.statusBarEndAlpha = 0
                    menu.presentationStyle.onTopShadowColor = .lightGray
                    menu.presentationStyle.onTopShadowRadius = 5
                    menu.presentationStyle.onTopShadowOpacity = 0.5
                    menu.presentationStyle.onTopShadowOffset = .zero
                    return menu
                }
                self?.present(menu, animated: true)
            }
        }
    }
    
    private func setupViewComponentsWithOutRequests(){
        let contairnerView = UIView()
        contairnerView.stack(contairnerView.stack(tittleLabel.withWidth(Size.width + 24),
                                                  lineBrownView.withSize(.init(width: Size.width + 32, height: 1)),
                                                  rectangleBrownView.withSize(.init(width: 40, height: 5)), spacing: 0, alignment: .leading),
                             viewClear.withSize(.init(width: Size.width, height: Size.width)),
                             spacing: 20,
                             alignment: .center)
        
        sscrolView.addSubview(contairnerView)
        contairnerView.anchor(top: sscrolView.topAnchor, leading: sscrolView.leadingAnchor, bottom: sscrolView.bottomAnchor, trailing: sscrolView.trailingAnchor)
        contairnerView.centerXTo(sscrolView.centerXAnchor)
        
        sscrolView.addSubview(newRequestButton.withHeight(30))
        newRequestButton.anchor(top: startRequestLabel.bottomAnchor, leading: sscrolView.leadingAnchor, bottom: nil, trailing: sscrolView.trailingAnchor, padding: .init(top: Size.width / 6, left: 48, bottom: 0, right: 48))
    }
    
    private func setupViewComponentsWithRequests(){
        let contairnerView = UIView()
        contairnerView.stack(contairnerView.stack(tittleLabel.withWidth(Size.width + 24),
                                                  lineBrownView.withSize(.init(width: Size.width + 32, height: 1)),
                                                  rectangleBrownView.withSize(.init(width: 40, height: 5)), spacing: 0, alignment: .leading),
                             contairnerView.stack(searchView.withWidth(Size.width),
                                                  requestTableView.withSize(.init(width: Size.width, height: CGFloat((requests.count + 1) * 45)))),
                             spacing: 20,
                             alignment: .center)
        
        sscrolView.addSubview(contairnerView)
        contairnerView.anchor(top: sscrolView.topAnchor, leading: sscrolView.leadingAnchor, bottom: sscrolView.bottomAnchor, trailing: sscrolView.trailingAnchor)
        contairnerView.centerXTo(sscrolView.centerXAnchor)
        
        sscrolView.addSubview(newRequestButton.withHeight(30))
        newRequestButton.anchor(top: requestTableView.bottomAnchor, leading: sscrolView.leadingAnchor, bottom: nil, trailing: sscrolView.trailingAnchor, padding: .init(top: 40, left: 32, bottom: 0, right: 32))
    }
    
    // MARK: - Selectors
    @objc private func toRegistrationApplication(){
        print("Evento funcional")
    }
}

extension RequestsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        requests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifierCell, for: indexPath) as! RequestTableViewCell
        cell.data = requests[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerCell) as! HeaderRequestCollectionReusableView
        return headerView
    }
    
}

extension RequestsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
}


extension RequestsViewController: MenuViewProtocol{
    func menuGoToController(destination: String) {
        GeneralFunctions.shareManager.gotToController(nombreControlador: destination, contexto: self)
    }
}
