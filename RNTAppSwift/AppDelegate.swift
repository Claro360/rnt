//
//  AppDelegate.swift
//  RNT
//
//  Created by Desarrollo iOS on 15/11/23.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import Firebase
import FirebaseMessaging


let firebaseApiKey = "AIzaSyC_xBpnmkLCGO-HJEmSg1Dwcf1P_pJf9VI"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey(firebaseApiKey)
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        setupFireBase(fileName: "GoogleService-Info")
        
        PemissionServicesSingleton.shareInstance.requestNotificationPermission()
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()

        if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
           let window = windowScene.windows.filter({ $0.isKeyWindow }).first {
            window.isMultipleTouchEnabled = true
        }
        
        return true
    }

    private func setupFireBase(fileName: String) {
        let filePath = Bundle.main.path(forResource: fileName, ofType: "plist")
        guard let fileOpts = FirebaseOptions(contentsOfFile: filePath!) else { return }
        FirebaseApp.configure(options: fileOpts)
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.reduce("") { $0 + String(format: "%02x", $1) }
        print("device token \(token)")
        Messaging.messaging().setAPNSToken(deviceToken as Data, type: .prod)
        /*if (SchemesSingleton.enviroment == 0) { //Comentario: Pendiente de agregar
              Messaging.messaging().setAPNSToken(deviceToken as Data, type: .sandbox)
          }else{
              Messaging.messaging().setAPNSToken(deviceToken as Data, type: .prod)
          }*/
    }
    
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}


extension AppDelegate: MessagingDelegate {
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("userInfo.AppDelegate: \(userInfo)")
        lauchNotification(userInfo)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken ?? "")")
        
        let dataDict: [String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    
    func lauchNotification(_ userInfo: [AnyHashable: Any]){
        let aux = UNMutableNotificationContent()
        if userInfo["title"] != nil {
            aux.title = "\(userInfo["title"] as? String ?? "Titulo")"
        }
        if userInfo["text"] != nil {
            aux.body = "\(userInfo["text"] as? String ?? "Desconocido")"
        }
        
        aux.badge = 0
        aux.sound = .default
        /*if UserDefaults.standard.bool(forKey: DefaultsConstants.notificationsActive) {
            aux.sound = .default
        }else{
            aux.sound = .none
        }*/
        let request = UNNotificationRequest(identifier: "Notificación", content: aux, trigger: nil)
        
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UNUserNotificationCenter.current().add(request){ error in
            if error != nil {
                print("Error en la notificación")
            }
        }
    }
}
