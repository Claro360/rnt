//
//  ViewController.swift
//  RNT
//
//  Created by Desarrollo iOS on 15/11/23.
//

import UIKit

let colorView = UIColor(named: "colorView")
let colorBackground = UIColor(named: "colorBackground")
let colorRnt = UIColor(named: "colorRnt")
let colorGreenRnt = UIColor(named: "colorGreenRnt")
let colorBrownRnt = UIColor(named: "colorBrownRnt")
let colorDarkYellow = UIColor(named: "ColorDarkYellow")
let colorGrayLayerTextInput = UIColor(named: "colorGrayLayerTextInput")
let colorGrayTextButton = UIColor(named: "colorGrayTextButton")
let colorGrayTextLabel = UIColor(named: "colorGrayTextLabel")
let colorGrayTextPlaceholder = UIColor(named: "colorGrayTextPlaceholder")
let revertedLabel = UIColor(named: "revertedLabel")

let devMod = true

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

struct Size {
    static let width: CGFloat = UIScreen.main.bounds.width - 64
    static let buttonPadding = UIEdgeInsets(top: 0, left: 64, bottom: 0, right: 64)
    static let height: CGFloat = (UIScreen.main.bounds.height / 4) * 3
}
