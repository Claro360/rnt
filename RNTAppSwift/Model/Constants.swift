//
//  Constants.swift
//  RNT
//
//  Created by Desarrollo iOS on 17/11/23.
//

import Foundation

struct Constants {
    static let aceptText = "Aceptar"
    static let cancelText = "Cancelar"
    static let onProgresText = "En progreso"
    static let startText = "Iniciar"
    static let selectOptionText = "Selecciona una opción"
    static let searchText = "Buscar"
    static let warningText = "Aviso"
    static let versionText = "Version"
    static let versionWithPointsText = "Version: "
    static let saveText = "Guardar"
    static let openConfigutationsText = "Es necesario el permiso de ubicación. Configuración -> Privacidad -> Localización"
    static let scanCodeQrText = "Escanear Código QR"
    static let logOutText = "Cerrar Sesión"
    static let startSearchText = "Iniciar una busqueda"
    
    //MARK: - LoginViewController
    static let welcomeToSECTURText = "¡Bienvenido a SECTUR!"
    static let getIntoWithPointsText = "Ingresar:"
    static let emailText = "Correo Electronico"
    static let emailWithPointsText = "Correo Electronico:"
    static let userText = "Usuario"
    static let userWithPointsText = "Usuario:"
    static let passwordText = "Contraseña"
    static let passwordWithPointsText = "Contraseña:"
    static let confirmPasswordText = "Confirmar Contraseña"
    static let confirmPasswordWithPointsText = "Confirmar Contraseña:"
    static let loginText = "Iniciar sesión"
    static let signInText = "Crear Cuenta"
    static let forgotPassword = "Olvide mi contraseña"
    
    
    //MARK: - SignInViewController
    static let signIn_LabelWithPointsText = "Registrate: "
    static let termsText = "Al registrarte aceptar los términos y condiciones del servicio y las políticas de privacidad."
    static let accountText = "¿Ya tienes cuenta?"
    static let recordText = "Registro"
    
    
    //MARK: - RecoverPasswordViewController
    static let recoverPasswordText = "Recuperar Contraseña"
    static let recoverText = "Recuperar"
    
    //MARK: - SelectionForLoginViewController
    static let loginAsWithPointsText = "Ingresar como:"
    static let tourisText = "Turista"
    static let guestText = "Invitado"
    static let travelServicesText = "Prestador de Servicios Turísticos"
    
    //MARK: - Onboardings Touris and Guest
    static let onboardingTittleText1 = "Prestadores de Servicios Turísticos"
    static let onboardingDescriptionText1 = "Consulta un extenso catálogo de Prestadoes de Servicios Turísticos, visualizandolo desde la ubicación en la que te encuentres o consultado el estado de tu interes."
    
    static let onboardingTittleText2 = "¿Quieres sabes de mas de un Prestador de Servicios Turísticos?"
    static let onboardingDescriptionText2 = "Ponemos a tu alcance seguridad, confianza y certeza con los PST, ofreciendote información acerca de ellos, así como: ubicación, datos de contacto y certificado de turismo."
    
    static let onboardingTittleText3 = "¡Agrega un Pestador de Servicios Turísticos a Favoritos!"
    static let onboardingDescriptionText3 = "Ten al alcance de a tu PST preverido y agrégalo a tu lista de favoritos fáxilmente, de esta manera lo tendras cuando más lo necesites."
    
    static let onboardingTittleText4 = "¡Consulta el Registro Nacional de Turismo de tu Prestador de Servicios Turísticos!"
    static let onboardingDescriptionText4 = "Realiza una consulta del RNT el certificado del PST, o escanea el código para validar si cetificación, de esta manera podrá dotae confianza y autenticidad al adquirir un serivicio turístico."
    
    //MARK: - Onboardings Travel Services
    static let onboardingTittleText5 = "¿Cómo ser un Prestador de Servicios Turísticos?"
    static let onboardingDescriptionText5 = "Realiza tu solicitud al Registro de Prestador de Servicios Turísticos, realizando el trámite de inscripción, actualización y rectificación de datos al RNT."
    
    static let onboardingTittleText6 = "¡Actualiza tu información en el RNT!"
    static let onboardingDescriptionText6 = "Como Prestador de Servicios Turísticos, deberás de mantener actualizada tu información, con la ubicación, contacto, documentación; entre algunas más para poder promover el incremento en los servicios turísticos y generando una mayor competitividad."
    
    static let onboardingTittleText7 = "¡Visibilidad en todo el país!"
    static let onboardingDescriptionText7 = "No importa tu ubicación, los estados y municipios podrán contar con la información correspondiente para conocerte y saber más acerca del PST."
    
    static let onboardingTittleText8 = "¡Prestador de Servicios Turísticos!"
    static let onboardingDescriptionText8 = "Se parte de las empresas que conforman la oferta turística nacional, generando confianza y autenticidad de los servicios turísticos que ofreces como PST.\n\n ¡Se parte de los PST ya mismo!"
    
    //MARK: - TravelServicesSignInViewController
    static let registrationProcessText = "Proceso de Inscripción al RNT"
    static let registerOnRntText = "Regístrate en el Registro Nacional de Turismo y obtén tu certificado RNT ya."
    static let tradenameText = "Nombre Comercial"
    static let tradenameWithPointsText = "Nombre Comercial:"
    static let typePSTText = "Tipo PST"
    static let typePSTWithPointsText = "Tipo PST:"
    static let typeOfPSTText = "Tipo de PST"
    static let typeOfPSTWithPointsText = "Tipo de PST:"
    static let categoryText = "Categoria"
    static let categoryWithPointsText = "Categoria: "
    static let categoriesText = "Categorias"
    static let categoriesWithPointsText = "Categorias: "
    static let namesText = "Nombre(s)"
    static let namesWithPointsText = "Nombre(s):"
    static let firstSurnameText = "Primer Apellido"
    static let firstSurnameWithPointsText = "Primer Apellido:"
    static let secondSurnameText = "Segundo Apellido"
    static let secondSurnameWithPointsText = "Segundo Apellido:"
    static let requiredFieldsText = "*Campos Obligatorios"
    static let signIn_LabelText = "Registrate"
    static let logingWithExistingAccountText = "Iniciar sesión con una cuenta existente"
    
    //MARK: - RequestsViewController
    static let requestRntText = "Solicitudes RNT"
    static let startRequestText = "Inicia tu primer solicitud"
    static let newRequestText = "Nueva Solicitud"
    static let toFilterText = "Ingrese criterio para filtrar resultado: "
    
    //MARK: - HomeViewController
    static let selectedLocation = "Ubicación seleccionada"
    static let addressText = "Domicilio"
    static let addressWithPointsText = "Domicilio: "
    static let contactText = "Contacto"
    static let contactWithPointsText = "Contacto: "
    static let socialsNetworksText = "Redes Sociales"
    static let socialsNetworksWithPointsText = "Redes Sociales: "
    static let certificateRntText = "Certificado RNT"
    
    //MARK: - FavoritesViewController
    static let myFavoritesText = "Mis Favoritos"
    static let cleanFavText = "Aquí visualizaras a tus\nPrestadores de Servicios Turísticos\nfavoritos."
    
    //MARK: - FiltersPSTViewController
    static let filtersText = "Filtros"
    static let locationsPSTText = "Ubicación de PST"
    static let selectPSTText = "Selecciona los PST que deseas Visualizar en el mapa."
    
    
    //MARK: - ScanCertificateViewController
    static let startScanerText = "Iniciar escaner"
    static let scanerFromImageText = "Escanear desde una imagen"
    
    //MARK: - MyProfileViewController
    static let myProfileText = "Mi Perfil"
    static let upDateProfileImageText = "Actualizar Imagen"
    static let upDateProfileDataText = "Actualizar Información"
    static let sexText = "Sexo"
    static let sexWithPointsText = "Sexo: "
    static let cellPhoneText = "Teléfono móvil"
    static let cellPhoneWithPointsText = "Teléfono móvil: "
    static let birthdateText = "Fecha de Nacimiento"
    static let birthdateWithPointsText = "Fecha de Nacimiento: "
    static let civilStatusText = "Estado Civil"
    static let civilStatusWithPointsText = "Estado Civil: "
    static let nationalityText = "Nacionalidad"
    static let nationalityWithPointsText = "Nacionalidad: "
    
    
    //MARK: -
    static let _Text = ""
    
    
    static let dumieLongText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vitae massa eu purus tincidunt tempus. Proin quis magna urna. Aenean at viverra diam. Aliquam laoreet libero a ipsum posuere rhoncus eu id mauris. Morbi vel fermentum nibh, in euismod lectus. Curabitur ac pellentesque neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In congue quis quam nec mollis. Etiam dapibus sodales tellus ac sollicitudin. Pellentesque aliquet enim ut vehicula euismod. Vestibulum eu gravida metus. Pellentesque ullamcorper faucibus varius. Nulla luctus tristique orci sit amet aliquam."
}
