//
//  modelFavorites.swift
//  RNT
//
//  Created by Desarrollo iOS on 30/11/23.
//

import Foundation

struct modelFavorites {
    let images: [String]?
    let name: String?
    let type: String?
    let direction: String?
    let buttonsContct: [String]?
    let favorit: Bool?
}
