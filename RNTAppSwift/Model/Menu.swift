//
//  Menu.swift
//  RNT
//
//  Created by Desarrollo iOS on 21/11/23.
//

enum MenuOptions: Int, CaseIterable {
    case pst
    case scan_Certificate
    case favorites
    case my_profile
    
    //case map //Comentario: Pendiente
  
    var description: [String] {
        switch self {
            
        case .pst:
            return ["Prestador de Servicios Turísticos", "pst"]
            
        case .scan_Certificate:
            return ["Escaneo de Certificado", "scanQr"]
            
        case .favorites:
            return ["Favoritos", "favoritesStar"]
            
        case .my_profile:
            return ["Mi Perfil", "myProfile"]
            
        /*case .map:
            return ["PST", "pst"]*/
            
        }
    }
}
