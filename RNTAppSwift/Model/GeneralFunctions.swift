//
//  GeneralFunctions.swift
//  RNT
//
//  Created by Desarrollo iOS on 21/11/23.
//

import UIKit
import ActionSheetPicker_3_0

class GeneralFunctions {
    static var shareManager = GeneralFunctions()
    
    func showAlert(title: String, message: String, actionDone: UIAlertAction, actionCancel: UIAlertAction?, context:UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(actionDone)
        if actionCancel != nil {
            alert.addAction(actionCancel!)
        }
        context.present(alert, animated: true, completion: nil)
    }
    
    func actionPermisosLocation(){
        if let appSettings = URL(string: UIApplication.openSettingsURLString + Bundle.main.bundleIdentifier!) {
            if UIApplication.shared.canOpenURL(appSettings) {
                UIApplication.shared.open(appSettings)
            }
        }
    }
    
    func openApp(AppName: String){
        switch AppName {
        case "icono_correo_electronico":
            if let url = URL(string: "mailto:suporte@ejemplo.com") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            break
            
        case "icono_facebook":
            if let url = URL(string: "fb://profile/{profile_id}") {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.open(URL(string: "https://www.facebook.com/{profile_id}")!, options: [:], completionHandler: nil)
                }
            }
            break
            
        case "icono_sitio_web":
            UIApplication.shared.open(URL(string: "https://www.google.com")!, options: [:], completionHandler: nil)
            break
        
        case "icono_telefono":
            if let url = URL(string: "tel://5512345678") {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
            break
        
        case "icono_tiktok":
            if let url = URL(string: "tiktok://user?screen_name=nombreUsuario") {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    if let webURL = URL(string: "https://www.tiktok.com/@nombreUsuario") {
                        UIApplication.shared.open(webURL, options: [:], completionHandler: nil)
                    }
                }
            }
            break
            
        case "icono_whatsapp":
            let phoneNumber = "5531537674"  //con el código de país?
            let whatsappURL = URL(string: "https://wa.me/\(phoneNumber)")!

            if UIApplication.shared.canOpenURL(whatsappURL) {
                UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
            } else {
                
                let appStoreURL = URL(string: "https://apps.apple.com/app/whatsapp-messenger/id310633997")!
                UIApplication.shared.open(appStoreURL, options: [:], completionHandler: nil)
            }
            break

        case "icono_x":
            if let url = URL(string: "twitter://user?screen_name=nombreUsuario") {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    if let webURL = URL(string: "https://twitter.com/nombreUsuario") {
                        UIApplication.shared.open(webURL, options: [:], completionHandler: nil)
                    }
                }
            }
            break
            
        default:
            break
        }
    }
    
    func getVersionApp () -> String {
        if devMod{
            return "\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "").dev\(Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")"
        }else{
            return "\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "")-\(Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "")"
        }
    }
    
    func pickerDataSelection(tittle: String, rows: [Any], sender: UIButton){
        ActionSheetStringPicker.show(withTitle: tittle, rows: rows, initialSelection: 0, doneBlock: { (picker, index, value) in
            if let option = value as? String {
                sender.setTitle(option, for: .normal)
            }
            print("value = ", String(describing: value) )
            print("index = ", String(describing: index) )
            
        }, cancel: { (picker) in
            return
        }, origin: sender)
    }
    
    func pickerDatesSelection(tittle: String, sender: UIButton){
        ActionSheetDatePicker(title: tittle, datePickerMode: .date, selectedDate: Date(), doneBlock: { (_, date, index) in
            if let date = date as? Date {
                let stringDate = DateFormatter.dateYYYYMMDD.string(from: date)
                sender.setTitle(stringDate, for: .normal)
                print("\nValue = ", String(describing: stringDate))
            }
        }, cancel: { (picker) in
            return
        }, origin: sender).show()
    }
    
    func gotToController(nombreControlador: String, contexto: UIViewController){
        print("gotToController: ", nombreControlador)
        switch nombreControlador {
        case "-3":

            GeneralFunctions.shareManager.transitionControllerRoot(destination: LoginViewController(toController: "SignInViewController"))
            break
        case "-2":
            GeneralFunctions.shareManager.transitionControllerRoot(destination: LoginViewController())
            break
        case "-1":
            print("Pendiente o.O")
            break
            
        case "0":
            if let navigationController = contexto.navigationController {
                if navigationController.topViewController is HomeViewController {
                    print("Estás en Mi Controlador")
                } else {
                    GeneralFunctions.shareManager.transitionControllerRoot(destination: HomeViewController())
                }
            }
            break
        case "1":
            if let navigationController = contexto.navigationController {
                if navigationController.topViewController is ScanCertificateViewController {
                    print("Estás en Mi Controlador")
                } else {
                    GeneralFunctions.shareManager.transitionControllerRoot(destination: ScanCertificateViewController())
                }
            }
            break
        case "2":
            if let navigationController = contexto.navigationController {
                if navigationController.topViewController is FavoritesViewController {
                    print("Estás en Mi Controlador")
                } else {
                    GeneralFunctions.shareManager.transitionControllerRoot(destination: FavoritesViewController())
                }
            }
            break
        case "3":
            if let navigationController = contexto.navigationController {
                if navigationController.topViewController is MyProfileViewController {
                    print("Estás en Mi Controlador")
                } else {
                    GeneralFunctions.shareManager.transitionControllerRoot(destination: MyProfileViewController())
                }
            }
            break
        default:
            print("destination: ", nombreControlador)
            break
        }
    }
    
    func transitionControllerRoot(destination: UIViewController){
        if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
           let window = windowScene.windows.filter({ $0.isKeyWindow }).first {

            let navigationController = UINavigationController(rootViewController: destination)
            navigationController.modalPresentationStyle = .fullScreen
            
            UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                window.rootViewController = navigationController
            }, completion: nil)
        }
    }
}
