//
//  requestsModel.swift
//  RNT
//
//  Created by Desarrollo iOS on 22/11/23.
//

import Foundation

struct requestsModel {
    var requestNumber: String?
    var tradename: String?
    var applicant: String?
    var pst: String?
    var categoryPST: String?
    var startDate: String?
    var updateDate: String?
    var status: String?
    var actions: String?
}
