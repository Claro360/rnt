//
//  DateFormatter.swift
//  RNT
//
//  Created by Desarrollo iOS on 04/12/23.
//

import UIKit

extension DateFormatter {
    static let dateYYYYMMDD: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd"
        return df
    }()
}
