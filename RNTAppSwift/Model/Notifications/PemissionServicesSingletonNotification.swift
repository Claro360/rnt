//
//  PemissionServicesSingletonNotification.swift
//  Claro360-Swift
//
//  Created by móviles 2 on 14/05/21.
//  Copyright © 2021 GlobalCorporation. All rights reserved.
//

import UIKit
import UserNotifications

extension PemissionServicesSingleton {
    //open app , i can get de information when recived a notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.list, .badge, .sound]);
        /*FBNotificationActionSingleton.shareInstance.calledOnForeground(userInfo: notification.request.content.userInfo)
        if UserDefaults.standard.bool(forKey: "notificationsActive") {
            completionHandler([.list, .badge, .sound]);
        }else{
            completionHandler([.list, .badge]);
        }*/
        
    }
    
    //open app , i can get de information when i touch the notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        guard let dictInfo = response.notification.request.content.userInfo as? [String: Any] else { return }
        //FBNotificationActionSingleton.shareInstance.goToDetailFromNotification(data: dictInfo)
        completionHandler();
    }
}
