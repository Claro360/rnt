//
//  PemissionServicesSingleton.swift
//  Claro360-Swift
//
//  Created by móviles 2 on 14/05/21.
//  Copyright © 2021 GlobalCorporation. All rights reserved.
//

import UIKit
import EventKit
import UserNotifications

enum PermissionRequest: String{
    case Notificaciones = "Notificaciones"
}

class PemissionServicesSingleton: NSObject, UNUserNotificationCenterDelegate {

    let eventStore = EKEventStore()
    let unCenter = UNUserNotificationCenter.current()
    static let shareInstance = PemissionServicesSingleton()
    
    
    func checkPermission(permissionRequest:PermissionRequest) -> Bool {
        switch permissionRequest {
        case .Notificaciones:
            let authorization = UIApplication.shared.isRegisteredForRemoteNotifications
            return authorization
        }
    }
    
    func requestNotificationPermission() -> Void{
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
           self.callNotificationPermision()
        }
    }
    
    private func callNotificationPermision(){
        let options: UNAuthorizationOptions = [ .alert, .badge, .sound ]
        unCenter.delegate = self
        unCenter.requestAuthorization(
            options: options,
            completionHandler: {_, _ in })
        
        DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
}
