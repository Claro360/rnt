//
//  MenuTableViewCell.swift
//  RNT
//
//  Created by Desarrollo iOS on 21/11/23.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    
    //MARK: - Properties
    lazy var cellImage: UIImageView = {
        let nextImage = UIImageView(frame: .zero)
        nextImage.translatesAutoresizingMaskIntoConstraints = false
        nextImage.image = UIImage(systemName: "chevron.right")?.withRenderingMode(.alwaysTemplate)
        nextImage.tintColor = .white
        return nextImage
    }()
    
    lazy var tittleLabel: UILabel = {
        let l = UILabel(frame: .zero)
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = .montserratRegular(ofSize: 12)
        l.textColor = .white
        l.numberOfLines = 0
        l.adjustsFontSizeToFitWidth = true
        return l
    }()

    //MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helpers
    fileprivate func setupCell() {
        self.backgroundColor = colorRnt
        
        self.addSubview(cellImage)
        self.addSubview(tittleLabel)
        
        cellImage.tintColor = .white
        
        NSLayoutConstraint.activate([
            cellImage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            cellImage.heightAnchor.constraint(equalToConstant: 20),
            cellImage.widthAnchor.constraint(equalToConstant: 20),
            cellImage.centerYAnchor.constraint(equalTo: tittleLabel.centerYAnchor),
            
            tittleLabel.leadingAnchor.constraint(equalTo: cellImage.trailingAnchor, constant: 10),
            tittleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
            tittleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
    }
    
    func setupDataCell(_ data: MenuOptions){
        tittleLabel.text = data.description[0]
        tittleLabel.numberOfLines = 0
        tittleLabel.adjustsFontSizeToFitWidth = true
        cellImage.image = UIImage(named: data.description[1])
        cellImage.tintColor = .white
    }
}
