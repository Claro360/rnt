//
//  Views.swift
//  RNT
//
//  Created by Desarrollo iOS on 29/11/23.
//

import UIKit

extension UIView {
    func roundedCorners(cornerRadius: CGFloat = 20, borderWith: CGFloat = 0, borderColor: UIColor = .lightGray, corners: CACornerMask = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner ]){
        self.layer.maskedCorners = corners                  //layerMinXMinYCorner - Top     Left
        self.layer.cornerRadius = cornerRadius              //layerMinXMaxYCorner - Botom   Left
        self.layer.borderWidth = borderWith                 //layerMaxXMaxYCorner - Bottom  Right
        self.layer.borderColor = borderColor.cgColor        //layerMaxXMinYCorner - Top     Right
    }
    
    func roundedView(cornerRadius: CGFloat, color: UIColor, borderWidth: CGFloat = 1, padding: CGFloat = 0) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color.cgColor
    }
    
    func addGradientColor(colors: [UIColor], vertial: Bool = true){
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors.map { $0.cgColor }
        gradientLayer.frame = bounds

        if vertial {
            gradientLayer.locations = [0.2, 1.0]
        }else{
            gradientLayer.startPoint = CGPoint(x: 0.2, y: 1)
            gradientLayer.endPoint =  CGPoint(x: 1, y: 1)
        }
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func addShadow(_ all: Bool = false){
        
        self.layer.shadowOpacity = 0.6
        self.layer.shadowColor = UIColor.lightGray.cgColor
        
        if all { //Agrega la sombra a los 4 lados de la view
            self.layer.shadowRadius = 4
            self.layer.shadowOffset = .zero
        }else{ //Agrega la sombra a los lados interior y derecho.
            self.layer.shadowRadius = 3
            self.layer.shadowOffset = CGSize(width: 3, height: 3)
        }
        self.layer.masksToBounds = false
    }
}
