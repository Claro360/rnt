//
//  ViewControllers.swift
//  RNT
//
//  Created by Desarrollo iOS on 21/11/23.
//

import UIKit

extension UIViewController {
    func setupNavBar(tittle: String, color: UIColor = .black, largeTittle: Bool = true){
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: color]
        navigationController?.navigationBar.largeTitleTextAttributes  = [.foregroundColor: color]
        navigationController?.navigationBar.tintColor = color
        navigationController?.navigationBar.backgroundColor = colorGreenRnt
        navigationController?.navigationBar.prefersLargeTitles = largeTittle
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barTintColor = colorGreenRnt
        navigationController?.navigationBar.layer.shadowOpacity = 0.2
        navigationController?.navigationBar.layer.shadowColor = UIColor.label.cgColor
        navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 5)
        
        navigationItem.title = tittle
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}
