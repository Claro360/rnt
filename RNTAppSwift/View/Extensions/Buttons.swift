//
//  Buttons.swift
//  RNT
//
//  Created by Desarrollo iOS on 15/11/23.
//

import UIKit

extension UIButton{
    convenience init(title: String, font: UIFont = .montserratRegular(ofSize: 12), target: Any? = nil, action: Selector? = nil) {
        self.init(type: .system)
        
        titleEdgeInsets = .init(top: 0, left: 10, bottom: 0, right: 34)
        setTitle(title, for: .normal)
        tintColor = .lightGray
        titleLabel?.font = font
        
        if let action = action {
            addTarget(target, action: action, for: .touchUpInside)
        }
    }
    
    func textUnderLined(){
        let attributes: [NSAttributedString.Key: Any] = [ .underlineStyle: NSUnderlineStyle.single.rawValue]
        let attributedText = NSAttributedString(string: self.currentTitle!, attributes: attributes)

        self.setAttributedTitle(attributedText, for: .normal)
    }
    
    func roundedButton(cornerRadius: CGFloat, color: UIColor, borderWidth: CGFloat = 1, contentAlignment: ContentHorizontalAlignment = .center, padding: CGFloat = 0) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color.cgColor
        self.contentHorizontalAlignment = contentAlignment
    }
    
}
