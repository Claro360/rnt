//
//  MaterialComponentsCustomOutlined.swift
//  RNT
//
//  Created by Desarrollo iOS on 15/11/23.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields
import MaterialComponents.MaterialTextControls_OutlinedTextAreas

extension MDCOutlinedTextField{
    func setupMDCOutlinedTextField(placeholder: String, labelTitle: String? = nil, placeholderColor: UIColor = colorGrayTextPlaceholder!, layerColor: UIColor = colorGrayLayerTextInput!, font: UIFont = .montserratBold(ofSize: 15)){
        self.backgroundColor = colorView
        self.textColor = .label
        self.addDoneButtonOnKeyboard()
        self.placeholder = placeholder
        self.sizeToFit()
        
        self.font = font
        
        self.setTextColor(.link, for: .editing)
        self.setTextColor(layerColor, for: .normal)
        self.setTextColor(.placeholderText, for: .disabled)
        
        self.setFloatingLabelColor(.link, for: .editing)
        self.setFloatingLabelColor(layerColor, for: .normal)
        self.setFloatingLabelColor(.lightGray, for: .disabled)
        
        self.setNormalLabelColor(placeholderColor, for: .normal)
        
        self.setOutlineColor(.link, for: .editing)
        self.setOutlineColor(layerColor, for: .normal)
        self.setOutlineColor(.lightGray, for: .disabled)
        if labelTitle != nil {
            if labelTitle == "igual" {
                self.label.text = placeholder
            }else {
                self.label.text = labelTitle
            }
        }
    }
    
    func setError(textError: String = "", colorError: UIColor = .red){
        self.setOutlineColor(colorError, for: MDCTextControlState.normal)
        self.setOutlineColor(colorError, for: MDCTextControlState.editing)
        self.setOutlineColor(colorError, for: MDCTextControlState.disabled)
        
        self.setFloatingLabelColor(colorError, for: .editing)
        self.setFloatingLabelColor(colorError, for: .normal)
        self.setFloatingLabelColor(colorError, for: .disabled)
        
        self.leadingAssistiveLabel.isHidden = true
        self.setLeadingAssistiveLabelColor(colorError, for: .disabled)
        self.setLeadingAssistiveLabelColor(colorError, for: .editing)
        self.setLeadingAssistiveLabelColor(colorError, for: .normal)
        if textError != "" {
            self.leadingAssistiveLabel.text = textError
            self.leadingAssistiveLabel.isHidden = false
        }else{
            self.leadingAssistiveLabel.text = ""
            self.leadingAssistiveLabel.isHidden = true
        }
    }
    
    func setOk(){
        self.setTextColor(.label, for: .normal)
        self.setTextColor(.label, for: .editing)
        self.setTextColor(.placeholderText, for: .disabled)
        
        self.setFloatingLabelColor(.link, for: .editing)
        self.setFloatingLabelColor(.label, for: .normal)
        self.setFloatingLabelColor(.lightGray, for: .disabled)
        
        self.setOutlineColor(.link, for: .editing)
        self.setOutlineColor(.label, for: .normal)
        self.setOutlineColor(.placeholderText, for: .disabled)
        
        self.setNormalLabelColor(.placeholderText, for: .normal)
        
        self.leadingAssistiveLabel.isHidden = true
        self.leadingAssistiveLabel.text = ""
    }
    
    func addButtonHidenPass() -> UIButton {
        let button = UIButton(type: .custom)
            button.frame = CGRect(x: CGFloat(0),
                                  y: CGFloat(0),
                                  width: CGFloat(self.frame.height),
                                  height: CGFloat(self.frame.height))
            self.trailingViewMode = .always
            self.isSecureTextEntry = true
            self.keyboardType = .asciiCapable
        button.tag = tag
        button.setImage(UIImage(systemName: "eye.fill")?.withTintColor(.lightGray), for: .normal)
        button.setImage(UIImage(systemName: "eye.slash.fill")?.withTintColor(.darkGray), for: .selected)
        button.addTarget(self, action: #selector(toggleVisibilityButtonTapped), for: .touchUpInside)
        button.tintColor = .lightGray
        return button
    }
    
    @objc func toggleVisibilityButtonTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.isSecureTextEntry = !sender.isSelected
    }
}

extension UITextField {
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Listo", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
}
