//
//  MaterialComponentsCustomFilled.swift
//  RNT
//
//  Created by Desarrollo iOS on 15/11/23.
//

import UIKit
import MaterialComponents.MaterialTextControls_FilledTextFields
import MaterialComponents.MaterialTextControls_FilledTextAreas

extension MDCFilledTextField{
    func setupMDCFilledTextField(placeholder: String, labelTitle: String? = nil){
        self.setFilledBackgroundColor(colorView!, for: .disabled)
        self.setFilledBackgroundColor(colorView!, for: .normal)
        self.setFilledBackgroundColor(colorView!, for: .editing)
        
        self.backgroundColor = colorView
        self.textColor = .label
        self.addDoneButtonOnKeyboard()
        self.placeholder = placeholder
        self.sizeToFit()
        self.attributedPlaceholder = NSAttributedString( string: placeholder,
                                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.placeholderText] )
        
        self.setFloatingLabelColor(.link, for: .editing)
        self.setFloatingLabelColor(.label, for: .normal)
        self.setFloatingLabelColor(.lightGray, for: .disabled)
        
        self.setNormalLabelColor(.placeholderText, for: .normal)
        
        self.setUnderlineColor(.link, for: .editing)
        self.setUnderlineColor(.label, for: .normal)
        self.setUnderlineColor(.lightGray, for: .disabled)
        if labelTitle != nil {
            if labelTitle == "igual" {
                self.label.text = placeholder
            }else {
                self.label.text = labelTitle
            }
        }
    }
    
    func setError(textError: String, colorError: UIColor){
        self.setFloatingLabelColor(colorError, for: .editing)
        self.setFloatingLabelColor(colorError, for: .normal)
        self.setFloatingLabelColor(colorError, for: .disabled)
        
        self.setUnderlineColor(colorError, for: MDCTextControlState.normal)
        self.setUnderlineColor(colorError, for: MDCTextControlState.editing)
        self.setUnderlineColor(colorError, for: MDCTextControlState.disabled)
        
        self.leadingAssistiveLabel.isHidden = false
        self.leadingAssistiveLabel.text = textError
        self.leadingAssistiveLabel.textColor = colorError
    }
    
    func setOk(){
        self.setFloatingLabelColor(.link, for: .editing)
        self.setFloatingLabelColor(.label, for: .normal)
        self.setFloatingLabelColor(.lightGray, for: .disabled)
        
        self.setNormalLabelColor(.placeholderText, for: .normal)
        
        self.setUnderlineColor(.link, for: .editing)
        self.setUnderlineColor(.label, for: .normal)
        self.setUnderlineColor(.lightGray, for: .disabled)
        
        self.leadingAssistiveLabel.isHidden = true
        self.leadingAssistiveLabel.text = ""
    }
}
