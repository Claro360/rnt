//
//  CustomNavBarView.swift
//  RNT
//
//  Created by Desarrollo iOS on 21/11/23.
//

import UIKit

class CustomNavBarView: UIView {
    // MARK: - PROPERTIES
    lazy var backButton = UIButton(image: (UIImage(systemName: "chevron.left")?.resizeImage(size: .init(width: 20, height: 20))!.withRenderingMode(.alwaysTemplate))!, tintColor: colorRnt!, target: self, action: #selector(backButtonTapped))
    lazy var tittleLabel = UILabel(text: "", font: .montserratBold(ofSize: 17), textColor: colorRnt!, textAlignment: .center, numberOfLines: 0)
    lazy var rigthButton = UIButton(image: (UIImage(systemName: "line.3.horizontal")?.resizeImage(size: .init(width: 30, height: 25))!.withRenderingMode(.alwaysTemplate))!, tintColor: colorRnt!, target: self, action: #selector(appointmentButtonTapped))
    
    var mx360ImageView =  UIImageView(image: UIImage(named: "mxImage"), contentMode: .scaleAspectFit )
    var tourisImageView = UIImageView(image: UIImage(named: "tourist"), contentMode: .scaleAspectFit)
    
    let bottonLineView = UIView(backgroundColor: colorRnt!)
    
    var withBottonLine = 0
    var buttonTappedHandler: ((String) -> Void)?
    
    // MARK: - INIT
    init(tittle: String, withBottonLine: Int = 0){
        super.init(frame: .zero)
        self.tittleLabel.text = tittle
        self.withBottonLine = withBottonLine
        backgroundColor = colorBrownRnt
        setUpViewComponents()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - HELPERS
    private func setUpViewComponents(){
        /*addSubview(backButton.withWidth(25).withHeight(25))
        addSubview(rigthButton.withWidth(30).withHeight(25))
        addSubview(tittleLabel)
        addSubview(mx360ImageView.withSize(.init(width: 100, height: 35)))
        addSubview(tourisImageView.withSize(.init(width: 120, height: 35)))
        
        mx360ImageView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: 8, left: 16, bottom: 8, right: 8))
        tourisImageView.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 8, left: 0, bottom: 0, right: 40))
        
        rigthButton.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 35, left: 0, bottom: 8, right: 8))*/
        
        tittleLabel.adjustsFontSizeToFitWidth = true
        
        if withBottonLine == 1{ //Solo Muestra el Titulo y Menú
            addSubview(rigthButton.withWidth(30).withHeight(25))
            addSubview(tittleLabel)
            tittleLabel.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 35, left: 40, bottom: 5, right: 40))
            rigthButton.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 35, left: 0, bottom: 8, right: 8))
            
        }else if withBottonLine == 2{ //Logos sin Texto Con Menú
            addSubview(rigthButton.withWidth(30).withHeight(25))
            addSubview(mx360ImageView.withSize(.init(width: 100, height: 35)))
            addSubview(tourisImageView.withSize(.init(width: 120, height: 35)))
            
            rigthButton.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 35, left: 0, bottom: 8, right: 8))
            mx360ImageView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: 8, left: 16, bottom: 8, right: 8))
            tourisImageView.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 8, left: 0, bottom: 0, right: 40))            
        }else{ //0: Texto con botón de regreso y Menú
            addSubview(backButton.withWidth(25).withHeight(25))
            addSubview(rigthButton.withWidth(30).withHeight(25))
            addSubview(tittleLabel)
            
            backButton.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: 35, left: 8, bottom: 5, right: 0))
            tittleLabel.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 35, left: 40, bottom: 5, right: 40))
            rigthButton.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 35, left: 0, bottom: 8, right: 8))
        }
        
        addSubview(bottonLineView.withHeight(1.5))
        bottonLineView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.6
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
    }
    
    // MARK: - SELECTORES
    @objc private func backButtonTapped(){
        buttonTappedHandler?("back")
    }
    
    @objc fileprivate func appointmentButtonTapped (){
        buttonTappedHandler?("menu")
    }
}
