//
//  HeaderRequestCollectionReusableView.swift
//  RNT
//
//  Created by Desarrollo iOS on 22/11/23.
//

import UIKit

class HeaderRequestCollectionReusableView: UITableViewHeaderFooterView {
    //MARK: - Properties
    let labelsCellId = "labelsCellId"
    lazy var labelsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.register(LabelsCollectionViewCell.self, forCellWithReuseIdentifier: self.labelsCellId)
        cv.dataSource = self
        cv.delegate = self
        cv.contentInset = .init(top: 0, left: 0, bottom: 0, right: 0)
        cv.backgroundColor = colorBackground
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    //MARK: - Init
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        self.backgroundColor = colorView
        setupCell()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helpers
    private func setupCell(){
        self.contentView.hstack(labelsCollectionView)
    }
}

extension HeaderRequestCollectionReusableView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: labelsCellId, for: indexPath) as! LabelsCollectionViewCell
        switch indexPath.row{
        case 0:
            cell.tittleCellLabel.text = "Número de solicitud"
            break
        case 1:
            cell.tittleCellLabel.text = "Nombre Comercial"
            break
        case 2:
            cell.tittleCellLabel.text = "Solicitante"
            break
        case 3:
            cell.tittleCellLabel.text = "PST"
            break
        case 4:
            cell.tittleCellLabel.text = "Categoria"
            cell.backgroundColor = colorRnt
            break
        case 5:
            cell.tittleCellLabel.text = "Fecha Inicio"
            break
        case 6:
            cell.tittleCellLabel.text = "Fecha de actualización"
            break
        case 7:
            cell.tittleCellLabel.text = "Estatus"
            break
        case 8:
            cell.tittleCellLabel.text = "Acciones"
            break
        case 9:
            cell.tittleCellLabel.text = "Extra?"
            break
        default:
            break
        }
        cell.tittleCellLabel.font = .montserratBold(ofSize: 7)
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.25
        return cell
    }
}

extension HeaderRequestCollectionReusableView: UICollectionViewDelegate {
    
}
