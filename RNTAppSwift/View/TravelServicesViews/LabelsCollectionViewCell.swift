//
//  LabelsCollectionViewCell.swift
//  RNT
//
//  Created by Desarrollo iOS on 22/11/23.
//

import UIKit

class LabelsCollectionViewCell: UICollectionViewCell {
    //MARK: - Properties
    let tittleCellLabel = UILabel(text: "", font: .montserratRegular(ofSize: 7), textColor: .label, textAlignment: .center, numberOfLines: 0)
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = colorView
        setupView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helpers
    private func setupView(){
        tittleCellLabel.adjustsFontSizeToFitWidth = true
        addSubview(tittleCellLabel)
        tittleCellLabel.centerInSuperview()
        tittleCellLabel.fillSuperview()
    }
}
