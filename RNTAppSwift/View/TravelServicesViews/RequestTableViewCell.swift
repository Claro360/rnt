//
//  RequestTableViewCell.swift
//  RNT
//
//  Created by Desarrollo iOS on 22/11/23.
//

import UIKit

class RequestTableViewCell: UITableViewCell {
    //MARK: - Properties
    var data: requestsModel?
    
    let labelsCellId = "labelsCellId"
    lazy var labelsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.register(LabelsCollectionViewCell.self, forCellWithReuseIdentifier: self.labelsCellId)
        cv.dataSource = self
        cv.delegate = self
        cv.contentInset = .init(top: 0, left: 0, bottom: 0, right: 0)
        cv.backgroundColor = colorBackground
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    //MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helpers
    private func setupCell(){
        self.contentView.hstack(labelsCollectionView)
    }

}

extension RequestTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: labelsCellId, for: indexPath) as! LabelsCollectionViewCell
        cell.backgroundColor = colorBackground
        switch indexPath.row{
        case 0:
            cell.tittleCellLabel.text = data?.requestNumber ?? "-"
            break
        case 1:
            cell.tittleCellLabel.text = data?.tradename ?? "-"
            break
        case 2:
            cell.tittleCellLabel.text = data?.applicant ?? "-"
            break
        case 3:
            cell.tittleCellLabel.text = data?.pst ?? "-"
            break
        case 4:
            cell.tittleCellLabel.text = data?.categoryPST ?? "-"
            cell.backgroundColor = colorRnt
            break
        case 5:
            cell.tittleCellLabel.text = data?.startDate ?? "-"
            break
        case 6:
            cell.tittleCellLabel.text = data?.updateDate ?? "-"
            break
        case 7:
            cell.tittleCellLabel.text = data?.status ?? "-"
            break
        case 8:
            cell.tittleCellLabel.text = data?.actions ?? "-"
            break
        case 9:
            cell.tittleCellLabel.text = "Extra?"
            break
        default:
            break
        }
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.25
        return cell
    }
}

extension RequestTableViewCell: UICollectionViewDelegate {
    
}
