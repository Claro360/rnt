//
//  Fonts.swift
//  RNT
//
//  Created by Desarrollo iOS on 16/11/23.
//

import Foundation
import UIKit

extension UIFont {
    private static func customFont(name: String, size: CGFloat) -> UIFont {
        let font = UIFont(name: name, size: size)
        assert(font != nil, "No se ha podido cargar la fuente: \(name)")
        return font ?? UIFont.systemFont(ofSize: size)
    }
    
    static func montserratAlternatesRegular(ofSize size: CGFloat) -> UIFont {
        return customFont(name: "MontserratAlternates-Regular", size: size)
    }
    
    static func montserratAlternatesBold(ofSize size: CGFloat) -> UIFont {
        return customFont(name: "MontserratAlternates-Bold", size: size)
    }
    
    static func montserratRegular(ofSize size: CGFloat) -> UIFont {
        return customFont(name: "Montserrat-Regular", size: size)
    }
    
    static func montserratBold(ofSize size: CGFloat) -> UIFont {
        return customFont(name: "Montserrat-Bold", size: size)
    }
    
    
    static func nunitoSansSemiBold(ofSize size: CGFloat) -> UIFont {
        return customFont(name: "NunitoSans-SemiBold", size: size)
    }
    
    static func NunitoSansBlack(ofSize size: CGFloat) -> UIFont {
        return customFont(name: "NunitoSans-Black", size: size)
    }
}
