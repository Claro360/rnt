//
//  ImagesCollectionViewCell.swift
//  RNT
//
//  Created by Desarrollo iOS on 30/11/23.
//

import UIKit
import SDWebImage

class ImagesCollectionViewCell: UICollectionViewCell {
    //MARK: - Properties
    let imageView = UIImageView(image: nil, contentMode: .scaleAspectFit)
    
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = colorView
        setupView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: - Helpers
    private func setupView() {
        contentView.addSubview(imageView)
        imageView.fillSuperview()
    }
    
    func setupImage(_ imageUrl: String?){
        imageView.sd_setImage(with: URL(string: imageUrl ?? "")){ [weak self] (image, error, cacheType, url)  in
            if error != nil {
                self?.imageView.image = UIImage(named: "LaunchS")
            }
        }
    }
}
