//
//  ContactButtonsCollectionViewCell.swift
//  RNT
//
//  Created by Desarrollo iOS on 01/12/23.
//

import UIKit

class ContactButtonsCollectionViewCell: UICollectionViewCell {
    //MARK: - Properties
    let contacImageButton = UIButton(image: UIImage(named: "icono_telefono")!)
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = colorView
        setupView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helpers
    private func setupView() {
        contentView.addSubview(contacImageButton)
        contacImageButton.roundedView(cornerRadius: 20, color: .lightGray)
        contacImageButton.fillSuperview()
    }
    
    func setupImage(_ imageName: String?){
        contacImageButton.setImage(UIImage(named: imageName!), for: .normal) //?.resizeImage(size: .init(width: 30, height: 30))?.withRenderingMode(.automatic ), for: .normal)
        contacImageButton.imageView?.contentMode = .scaleAspectFill
        contacImageButton.isUserInteractionEnabled = false
    }
    
    /*MARK: - Properties
    let imageView = UIImageView(image: nil, contentMode: .scaleAspectFit)
    
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = colorView
        setupView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: - Helpers
    private func setupView() {
        contentView.addSubview(imageView)
        imageView.roundedView(cornerRadius: 20, color: .lightGray)
        imageView.fillSuperview()
    }
    
    func setupImage(_ imageName: String?){
        imageView.image =  UIImage(named: imageName!) //?.resizeImage(size: .init(width: 30, height: 30))?.withRenderingMode(.automatic ), for: .normal)
        //contacImageButton.imageView?.contentMode = .scaleAspectFill
    }*/
}
