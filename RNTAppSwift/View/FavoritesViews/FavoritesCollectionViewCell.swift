//
//  FavoritesCollectionViewCell.swift
//  RNT
//
//  Created by Desarrollo iOS on 30/11/23.
//

import UIKit

class FavoritesCollectionViewCell: UICollectionViewCell {
    //MARK: - Properties
    let imagesCellId = "imagesCellId"
    lazy var imagesCollectionView: UICollectionView = {
        let ly = UICollectionViewFlowLayout()
        ly.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: ly)
        cv.backgroundColor = colorGreenRnt
        cv.register(ImagesCollectionViewCell.self, forCellWithReuseIdentifier: imagesCellId)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    var imagesArray: [String]? {
        didSet{
            imagesCollectionView.reloadData()
        }
    }
    var buttonsaArray: [String]?{
        didSet{
            contactsCollectionView.reloadData()
        }
    }
    
    lazy var nameButton = UIButton(title: "", titleColor: colorRnt!, font: .montserratBold(ofSize: 18), target: self, action: #selector(toDetailsPST))
    lazy var favButton = UIButton(image: UIImage(systemName: "star.fill")!, target: self, action: #selector(quitFromFavorits))
    let typeLabel = UILabel(font: .montserratRegular(ofSize: 14), textColor: colorGrayTextLabel!, textAlignment: .left, numberOfLines: 0)
    lazy var directionButton = UIButton(title: "", titleColor: .link, font: .montserratBold(ofSize: 14), target: self, action: #selector(toRouteOnMaps))
    let divLineView = UIView(backgroundColor: .lightGray)
    
    let contactButtonCellId = "contactButtonCellId"
    lazy var contactsCollectionView: UICollectionView = {
        let ly = UICollectionViewFlowLayout()
        ly.scrollDirection = .horizontal
        ly.minimumLineSpacing = 5
        let cv = UICollectionView(frame: .zero, collectionViewLayout: ly)
        cv.backgroundColor = .clear
        cv.register(ContactButtonsCollectionViewCell.self, forCellWithReuseIdentifier: contactButtonCellId)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    var handlerCellFavorits: ((String, Int) -> Void)?
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = colorView
        setupView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Heplers
    private func setupView(){
        favButton.tintColor = .systemYellow
        favButton.imageView?.fillSuperview()
        
        contentView.addSubview(imagesCollectionView.withHeight(120))
        contentView.addSubview(nameButton)
        contentView.addSubview(favButton.withSize(.init(width: 35, height: 30)))
        contentView.addSubview(typeLabel)
        contentView.addSubview(directionButton)
        contentView.addSubview(divLineView.withHeight(1.5))
        contentView.addSubview(contactsCollectionView.withHeight(40))
        
        imagesCollectionView.anchor(top: self.topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .allSides(10))
        nameButton.anchor(top: imagesCollectionView.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 10, bottom: 0, right: 30))
        favButton.anchor(top: imagesCollectionView.bottomAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 8))
        typeLabel.anchor(top: nameButton.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 4, left: 10, bottom: 4, right: 10))
        directionButton.anchor(top: typeLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 10, bottom: 10, right: 10))
        divLineView.anchor(top: directionButton.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 8, bottom: 0, right: 8))
        contactsCollectionView.anchor(top: divLineView.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 10, left: 10, bottom: 10, right: 10))
    }
    
    func setupDataCell(_ data: modelFavorites){
        nameButton.setTitle(data.name, for: .normal)
        nameButton.contentHorizontalAlignment = .left
        favButton.isHidden = !(data.favorit ?? true)
        typeLabel.text = data.type
        directionButton.setTitle(data.direction, for: .normal)
        directionButton.contentHorizontalAlignment = .leading
        directionButton.titleLabel?.numberOfLines = 0
        directionButton.titleLabel?.adjustsFontSizeToFitWidth = true
        imagesArray = data.images ?? []
        buttonsaArray = data.buttonsContct ?? []
    }
    
    //MARK: Selectors
    @objc private func quitFromFavorits(){
        handlerCellFavorits?("F", -1)
    }
    @objc private func toDetailsPST(){
        handlerCellFavorits?("D", -1)
    }
    @objc private func toRouteOnMaps(){
        handlerCellFavorits?("M", -1)
    }
}

//MARK: - UICollectionViewDataSource
extension FavoritesCollectionViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == imagesCollectionView{
            return imagesArray?.count ?? 0
        }else{
            return buttonsaArray?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == imagesCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imagesCellId, for: indexPath) as! ImagesCollectionViewCell
            cell.setupImage(imagesArray?[indexPath.item])
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: contactButtonCellId, for: indexPath) as! ContactButtonsCollectionViewCell
            cell.setupImage(buttonsaArray?[indexPath.item])
            return cell
        }
            
    }
    
    
}
//MARK: - UICollectionViewDelegate
extension FavoritesCollectionViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == imagesCollectionView {
            handlerCellFavorits?("Imgs", indexPath.item)
        }else if collectionView == contactsCollectionView {
            handlerCellFavorits?(buttonsaArray?[indexPath.item] ?? "---", -1)
        }
    }
    
}
//MARK: - UICollectionViewDelegateFlowLayout
extension FavoritesCollectionViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == imagesCollectionView {
            return .init(width: 120, height: 120)
        }else{
            return .init(width: 40, height: 40)
        }
    }
}
