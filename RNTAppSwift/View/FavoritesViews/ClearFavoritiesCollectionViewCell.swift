//
//  ClearFavoritesCollectionViewCell.swift
//  RNT
//
//  Created by Desarrollo iOS on 12/12/23.
//

import UIKit

class ClearFavoritesCollectionViewCell: UICollectionViewCell {
    //MARK: Properties
    let favClearImageView = UIImageView(image: UIImage(named: "favClearImage"))
    let descriptionClearLabel = UILabel(text: Constants.cleanFavText, font: .montserratRegular(ofSize: 16), textColor: colorGrayTextLabel!, textAlignment: .center, numberOfLines: 0)
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = colorBackground
        setupView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Helpers
    private func setupView(){
        self.roundedView(cornerRadius: 10, color: .darkGray, borderWidth: 0.75)
        self.addSubview(favClearImageView.withSize(.init(width: Size.width / 2, height: Size.width / 2)))
        favClearImageView.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil, padding: .allSides(30))
        favClearImageView.centerXToSuperview()
        
        self.addSubview(descriptionClearLabel)
        descriptionClearLabel.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .allSides(20))
        self.addShadow(true)
    }
}
