//
//  MarkersTableViewCell.swift
//  RNT
//
//  Created by Desarrollo iOS on 07/12/23.
//

import UIKit

class MarkersTableViewCell: UITableViewCell {
    //MARK: - Properties
    let titleLabelCell = UILabel(font: .montserratRegular(ofSize: 12), textColor: colorGrayTextLabel!, textAlignment: .left, numberOfLines: 0)
    private let onOffSwitch = UISwitch()
    
    //MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helpers
    private func setupCell(){
        self.backgroundColor = colorBackground
        onOffSwitch.addTarget(self, action: #selector(switchChanged(_:)), for: .valueChanged)
        onOffSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        onOffSwitch.onTintColor = colorRnt
        self.imageView!.image = UIImage(named: "01_agencia_de_viajes")
        
        self.addSubview(titleLabelCell)
        titleLabelCell.anchor(top: contentView.topAnchor, leading: self.imageView?.trailingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor, padding: .init(top: 2, left: 4, bottom: 2, right: 4))
        
        //self.detailTextLabel?.text = "Detales aux"
        //self.textLabel?.text = "ukhuogy"
        //self.detailTextLabel?.text = "asdfghjk"
        self.separatorInset = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        self.accessoryView = onOffSwitch

    }

    @objc func switchChanged(_ sender: UISwitch) {
        if sender.isOn {
            print("Switch activado")
        } else {
            print("Switch desactivado")
        }
    }
    
    func setupDatCell(tittle: String, imageName: String){
        self.titleLabelCell.text = tittle
        self.titleLabelCell.numberOfLines = 0
        self.titleLabelCell.adjustsFontSizeToFitWidth = true
        self.imageView?.image = UIImage(named: imageName)
    }


    /*override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }*/

}
