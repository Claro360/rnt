//
//  InfoWindowView.swift
//  RNT
//
//  Created by Desarrollo iOS on 08/12/23.
//

import UIKit

class InfoWindowView: UIView {
    //MARK: - Properties
    private lazy var closeMenuButton = UIButton(image: UIImage(systemName: "xmark")!, tintColor: .white, target: self, action: #selector(closeMenuSelector))
    
    private let sscrollView = UIScrollView()
    
    private lazy var nameButton = UIButton(title: "", titleColor: colorRnt!, font: .montserratBold(ofSize: 18), target: self, action: #selector(toDetailsPST))
    private let typeLabel = UILabel(font: .montserratBold(ofSize: 14), textColor: colorGrayTextLabel!, textAlignment: .left, numberOfLines: 0)
    private let textAddressLabel = UILabel(text: Constants.addressWithPointsText, font: .montserratBold(ofSize: 14), textColor: colorGrayTextLabel!, textAlignment: .left, numberOfLines: 0)
    private let addressLabel = UILabel(font: .montserratRegular(ofSize: 12), textColor: colorGrayTextLabel!, textAlignment: .left, numberOfLines: 0)
    
    private let dividerLine1 = UIView(backgroundColor: .lightGray)
    private let textContactsLabel = UILabel(text: Constants.contactWithPointsText, font: .montserratBold(ofSize: 14), textColor: colorGrayTextLabel!, textAlignment: .left, numberOfLines: 0)
    private let contactscellId = "contactscellId"
    private lazy var contactsCollectionView: UITableView = {
        let tv = UITableView()
        tv.dataSource = self
        tv.delegate = self
        tv.register(MarkersTableViewCell.self, forCellReuseIdentifier: contactscellId)
        tv.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tv.backgroundColor = .clear
        tv.separatorStyle = .none
        return tv
    }()
    var contactsArray: [(String, String)]? {
        didSet{
            contactsCollectionView.reloadData()
        }
    }
    
    private let dividerLine2 = UIView(backgroundColor: .lightGray)
    private let textSocialNetworksLabel = UILabel(text: Constants.socialsNetworksWithPointsText, font: .montserratBold(ofSize: 14), textColor: colorGrayTextLabel!, textAlignment: .left, numberOfLines: 0)
    private let sNcellId = "pstscellId"
    private lazy var socialNetworksCollectionView: UITableView = {
        let tv = UITableView()
        tv.dataSource = self
        tv.delegate = self
        tv.register(MarkersTableViewCell.self, forCellReuseIdentifier: sNcellId)
        tv.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tv.backgroundColor = .clear
        tv.separatorStyle = .none
        return tv
    }()
    var socialNetworksArray: [(String, String)]?{
        didSet{
            socialNetworksCollectionView.reloadData()
        }
    }

    
    private let dividerLine3 = UIView(backgroundColor: .lightGray)
    private let imagesCellId = "pstscellId"
    lazy var imagesCollectionView: UICollectionView = {
        let ly = UICollectionViewFlowLayout()
        ly.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: ly)
        cv.backgroundColor = colorGreenRnt
        cv.register(ImagesCollectionViewCell.self, forCellWithReuseIdentifier: imagesCellId)
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    var imagesArray: [String]?{
        didSet{
            socialNetworksCollectionView.reloadData()
        }
    }
    
    
    var infoWindowHandler: ((String) -> Void)?
    
    //MARK: - Init
    init(data: modelFavorites){
        super.init(frame: .zero)
        
        nameButton.setTitle(data.name, for: .normal)
        nameButton.contentHorizontalAlignment = .left
        typeLabel.text = data.type
        addressLabel.text = data.direction
        addressLabel.numberOfLines = 0
        contactsArray = [("icono_telefono", "5512345678"), ("icono_whatsapp", "5598765432"), ("icono_correo_electronico", "demo@mail.com"), ("icono_sitio_web", "https://www.facebook.com/{profile_id}")]
        socialNetworksArray = [("icono_facebook", "https://www.facebook.com/{profile_id}"), ("icono_x", "https://twitter.com/nombreUsuario"), ("icono_tiktok", "https://www.tiktok.com/@nombreUsuario")]
        imagesArray = data.images
        
        backgroundColor = colorBrownRnt
        setUpViewComponents()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    //MARK: - Helpers
    private func setUpViewComponents(){
        closeMenuButton.roundedView(cornerRadius: 10, color: .red)
        closeMenuButton.backgroundColor = .red

        sscrollView.addSubview(nameButton)
        sscrollView.addSubview(typeLabel)
        sscrollView.addSubview(textAddressLabel)
        sscrollView.addSubview(addressLabel)
        sscrollView.addSubview(dividerLine1.withSize(.init(width: sscrollView.frame.width, height: 1.5)))
        sscrollView.addSubview(textContactsLabel)
        sscrollView.addSubview(contactsCollectionView.withHeight(CGFloat((contactsArray?.count ?? 0) * 30)))
        sscrollView.addSubview(dividerLine2.withSize(.init(width: sscrollView.frame.width, height: 1.5)))
        sscrollView.addSubview(textSocialNetworksLabel)
        sscrollView.addSubview(socialNetworksCollectionView.withHeight(CGFloat((socialNetworksArray?.count ?? 0) * 30)))
        sscrollView.addSubview(dividerLine3.withSize(.init(width: sscrollView.frame.width, height: 1.5)))
        sscrollView.addSubview(imagesCollectionView.withHeight(60))
        
        nameButton.centerXTo(sscrollView.centerXAnchor)
        
        nameButton.anchor(top: sscrollView.topAnchor, leading: sscrollView.leadingAnchor, bottom: nil, trailing: sscrollView.trailingAnchor, padding: .init(top: 30, left: 8, bottom: 0, right: 8))
        typeLabel.anchor(top: nameButton.bottomAnchor, leading: sscrollView.leadingAnchor, bottom: nil, trailing: sscrollView.trailingAnchor, padding: .init(top: 0, left: 8, bottom: 10, right: 8))
        textAddressLabel.anchor(top: typeLabel.bottomAnchor, leading: sscrollView.leadingAnchor, bottom: nil, trailing: sscrollView.trailingAnchor, padding: .init(top: 10, left: 8, bottom: 10, right: 8))
        addressLabel.anchor(top: textAddressLabel.bottomAnchor, leading: sscrollView.leadingAnchor, bottom: nil, trailing: sscrollView.trailingAnchor, padding: .init(top: 0, left: 8, bottom: 10, right: 8))
        dividerLine1.anchor(top: addressLabel.bottomAnchor, leading: sscrollView.leadingAnchor, bottom: nil, trailing: sscrollView.trailingAnchor, padding: .init(top: 10, left: 8, bottom: 10, right: 8))
        textContactsLabel.anchor(top: dividerLine1.bottomAnchor, leading: sscrollView.leadingAnchor, bottom: nil, trailing: sscrollView.trailingAnchor, padding: .init(top: 10, left: 8, bottom: 10, right: 8))
        contactsCollectionView.anchor(top: textContactsLabel.bottomAnchor, leading: sscrollView.leadingAnchor, bottom: nil, trailing: sscrollView.trailingAnchor, padding: .init(top: 10, left: 8, bottom: 10, right: 8))
        dividerLine2.anchor(top: contactsCollectionView.bottomAnchor, leading: sscrollView.leadingAnchor, bottom: nil, trailing: sscrollView.trailingAnchor, padding: .init(top: 10, left: 8, bottom: 10, right: 8))
        textSocialNetworksLabel.anchor(top: dividerLine2.bottomAnchor, leading: sscrollView.leadingAnchor, bottom: nil, trailing: sscrollView.trailingAnchor, padding: .init(top: 10, left: 8, bottom: 10, right: 8))
        socialNetworksCollectionView.anchor(top: textSocialNetworksLabel.bottomAnchor, leading: sscrollView.leadingAnchor, bottom: nil, trailing: sscrollView.trailingAnchor, padding: .init(top: 10, left: 8, bottom: 10, right: 8))
        dividerLine3.anchor(top: socialNetworksCollectionView.bottomAnchor, leading: sscrollView.leadingAnchor, bottom: nil, trailing: sscrollView.trailingAnchor, padding: .init(top: 10, left: 8, bottom: 10, right: 8))
        imagesCollectionView.anchor(top: dividerLine3.bottomAnchor, leading: sscrollView.leadingAnchor, bottom: sscrollView.bottomAnchor, trailing: sscrollView.trailingAnchor, padding: .init(top: 10, left: 8, bottom: 10, right: 8))
        
        sscrollView.roundedView(cornerRadius: 5, color: .lightGray)
        
        addSubview(sscrollView)
        sscrollView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .allSides(8))
        sscrollView.centerXTo(centerXAnchor)
        sscrollView.showsVerticalScrollIndicator = false
        
        addSubview(closeMenuButton.withSize(.init(width: 20, height: 20)))
        closeMenuButton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .allSides(10))
    }
    
    //MARK: - Selectors
    @objc private func closeMenuSelector(){
        infoWindowHandler?("close")
    }
    
    @objc private func toDetailsPST(){
        infoWindowHandler?("ToDetails")
    }

}

//MARK: - UITableViewDataSource
extension InfoWindowView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == contactsCollectionView {
            return contactsArray!.count
        }else{
            return socialNetworksArray!.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == contactsCollectionView {
            let cell = tableView.dequeueReusableCell(withIdentifier: contactscellId, for: indexPath) as! MarkersTableViewCell
            cell.setupDatCell(tittle: contactsArray![indexPath.row].1, imageName: contactsArray![indexPath.row].0)
            cell.accessoryView = .none
            cell.backgroundColor = .clear
            cell.titleLabelCell.textColor = .link
            cell.titleLabelCell.font = .montserratRegular(ofSize: 10)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: sNcellId, for: indexPath) as! MarkersTableViewCell
            cell.setupDatCell(tittle: socialNetworksArray![indexPath.row].1, imageName: socialNetworksArray![indexPath.row].0)
            cell.accessoryView = .none
            cell.backgroundColor = .clear
            cell.titleLabelCell.textColor = .link
            cell.titleLabelCell.font = .montserratRegular(ofSize: 9)
            return cell
        }
        
    }
}
//MARK: - UITableViewDelegate
extension InfoWindowView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
}

//MARK: - UICollectionViewDataSource
extension InfoWindowView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imagesCellId, for: indexPath) as! ImagesCollectionViewCell
        cell.setupImage(imagesArray?[indexPath.item])
        cell.backgroundColor = .clear
        return cell
    }
}
extension InfoWindowView: UICollectionViewDelegate{
    
}
extension InfoWindowView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 60, height: 60)
    }
}
